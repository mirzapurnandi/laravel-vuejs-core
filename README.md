```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:mirzapurnandi/laravel-vuejs-core.git
or 
git remote add origin https://gitlab.com/mirzapurnandi/laravel-vuejs-core.git
```

- COPY file .env.example (cp .env.example .env)
- BUAT SEBUAH DATABASE BERNAMA db_testonline2 di file .env
- composer install

## PHP ARTISAN
- php artisan key:generate
- php artisan migrate
- php artisan db:seed

## Menambahkan beberapa componen tambahan
- npm install
- npm install vuex --save
- npm install vue-router
- npm i vue bootstrap-vue bootstrap
- npm install axios
- npm run dev atau npm run watch (jalankan jika semua sudah selesai di edit)

## MENAMBAHKAN ROLES DAN PERMISSION
1. roles => php artisan permission:create-role superadmin
2. permission => php artisan permission:create-permission "read-user"
3. permission => php artisan permission:create-permission "create-user"