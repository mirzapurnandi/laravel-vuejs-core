<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'Auth\LoginController@login');

Route::post('userregister', 'API\UserController@userregister')->middleware('check-token');
Route::get('testing', 'API\SoalTestIQController@testing');
//Route::get('testing', 'API\HomeController@testing');

Route::group(['middleware' => 'auth:api'], function() {
	Route::get('user-authenticated', 'API\UserController@getUserLogin')->name('user.authenticated');
	Route::get('user-lists', 'API\UserController@userLists')->name('user.index');

	Route::get('roles', 'API\RolePermissionController@getAllRole')->name('roles');
	Route::get('permissions', 'API\RolePermissionController@getAllPermission')->name('permission');
	Route::post('role-permission', 'API\RolePermissionController@getRolePermission')->name('role_permission');
	Route::post('set-role-permission', 'API\RolePermissionController@setRolePermission')->name('set_role_permission');
	Route::post('set-role-user', 'API\RolePermissionController@setRoleUser')->name('user.set_role');

	Route::resource('/jobseekers', 'API\UserController')->except(['create', 'show', 'update']);
	Route::post('/jobseekers/{id}', 'API\UserController@update')->name('jobseekers.update');
	Route::get('/jobseekers/test-list/{id}', 'API\UserController@get_test_list')->name('jobseekers.get_test_list');
	Route::post('/jobseekers/test-list/set', 'API\UserController@set_test_list')->name('jobseekers.set_test_list');
	Route::get('/jobseekers/test-list/view-scores/{id}', 'API\UserController@get_score_list_of_test')->name('jobseekers.get_score_list_of_test');

	Route::resource('/soaltestiqs', 'API\SoalTestIQController')->except(['create', 'show', 'update']);
	Route::post('/soaltestiqs/{id}', 'API\SoalTestIQController@update')->name('soaltestiqs.update');
	Route::get('/soaltestiqs/penilaiantestiq/{id}', 'API\SoalTestIQController@penilaiantestiq')->name('soaltestiqs.penilaiantestiq');
	Route::get('/soaltestiqs/prosestestiq/{id}', 'API\SoalTestIQController@prosestestiq')->name('soaltestiqs.prosestestiq');
    
    Route::get('/homes', 'API\HomeController@index')->name('homes');
    Route::get('/homes/{id}/edit', 'API\HomeController@edit')->name('homes.edit');
    Route::get('/homes/ujian/{id}', 'API\HomeController@ujian')->name('homes.ujian');
    Route::post('/homes/ujian2', 'API\HomeController@ujian2')->name('homes.ujian2');
    Route::post('/homes/jawaban', 'API\HomeController@jawaban')->name('homes.jawaban');
    Route::get('/homes/selesai/{id}', 'API\HomeController@selesai')->name('homes.selesai');

    Route::post('/homes/start-test', 'API\HomeController@startTest')->name('homes.startTest');
    Route::post('/homes/finish-test', 'API\HomeController@finishTest')->name('homes.finishTest');
    //Route::get('/soaltestiq/laundry-type', 'API\SoalTestIQController@show');
    //Route::post('/soaltestiq/laundry-type', 'API\SoalTestIQController@show');

    Route::resource('/papi-kostick-test', 'API\PapiKostickTestQuestionController')->except(['create', 'show']);

});

// Route::resource('/soaltestiqs', 'API\SoalTestIQController')->except(['create', 'show', 'update']);
// Route::resource('/papi-kostick-test', 'API\PapiKostickTestQuestionController')->except(['create', 'show']);
// Route::get('/jobseekers/test-list/{id}', 'API\UserController@test_list')->name('jobseekers.test_list');
