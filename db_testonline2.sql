-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 22 Nov 2019 pada 08.04
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_testonline2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_10_23_082312_create_permission_tables', 1),
(5, '2019_10_24_095320_create_ujian_jeniss_table', 1),
(6, '2019_10_25_024722_create_user_ujians_table', 1),
(9, '2019_10_28_082853_create_soal_testiqs_table', 2),
(16, '2019_11_04_061357_create_soal_testiq_temps_table', 3),
(17, '2019_11_12_100419_create_soal_testiq_rumus_table', 4),
(18, '2019_11_18_092138_create_soal_testiq_jenis_table', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'read-user', 'web', '2019-10-27 19:12:31', '2019-10-27 19:12:31'),
(2, 'create-user', 'web', '2019-10-27 19:12:42', '2019-10-27 19:12:42'),
(3, 'edit-user', 'web', '2019-10-27 19:12:49', '2019-10-27 19:12:49'),
(4, 'delete-user', 'web', '2019-10-27 19:12:54', '2019-10-27 19:12:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'web', '2019-10-27 19:10:39', '2019-10-27 19:10:39'),
(2, 'admin', 'web', '2019-10-27 19:11:29', '2019-10-27 19:11:29'),
(3, 'employer', 'web', '2019-10-27 19:11:52', '2019-10-27 19:11:52'),
(4, 'jobseeker', 'web', '2019-10-27 19:12:01', '2019-10-27 19:12:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `soal_testiqs`
--

CREATE TABLE `soal_testiqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `soal` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `soal_gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pilihan_a` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pilihan_a_gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pilihan_b` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pilihan_b_gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pilihan_c` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pilihan_c_gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pilihan_d` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pilihan_d_gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pilihan_e` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pilihan_e_gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pilihan_f` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pilihan_f_gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jawaban` enum('a','b','c','d','e','f') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `jenis` enum('1','2','3','4') COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `soal_testiqs`
--

INSERT INTO `soal_testiqs` (`id`, `soal`, `soal_gambar`, `pilihan_a`, `pilihan_a_gambar`, `pilihan_b`, `pilihan_b_gambar`, `pilihan_c`, `pilihan_c_gambar`, `pilihan_d`, `pilihan_d_gambar`, `pilihan_e`, `pilihan_e_gambar`, `pilihan_f`, `pilihan_f_gambar`, `jawaban`, `created_at`, `updated_at`, `jenis`) VALUES
(1, 'Soal testing', NULL, 'Pilihan 1', NULL, 'Pilihan 2', NULL, 'Pilihan 3', NULL, 'Pilihan 4', NULL, 'Pilihan 5', NULL, NULL, NULL, 'a', '2019-10-29 01:34:42', '2019-10-29 01:34:42', '1'),
(3, 'Soal disini untuk ditampilkan 3', NULL, 'Jawaban A 3', NULL, 'Jawaban B 3', NULL, 'Jawaban C 3', NULL, 'Jawaban D 3', NULL, 'Jawaban E 3', NULL, NULL, NULL, 'a', '2019-10-29 01:37:41', '2019-10-29 02:59:38', '1'),
(4, 'Tambah soal ketiga untuk ujicoba ujian', NULL, 'mantap', NULL, 'mantap sekali', NULL, 'sekali aja mantap', NULL, 'oke oke', NULL, 'bebas', NULL, NULL, NULL, 'a', '2019-10-30 00:38:38', '2019-10-30 00:38:38', '1'),
(5, 'Soal ke empat adalah ujian terberat', NULL, 'Angkat Batu', NULL, 'Angkat Meja', NULL, 'Angkat Barbel', NULL, 'Angkat Besi', NULL, 'Angkatan', NULL, NULL, NULL, 'a', '2019-10-30 00:39:20', '2019-10-30 00:39:20', '1'),
(7, 'Soal ke enam menampilkan beberapa data', NULL, 'siap siap', NULL, 'mantap bro', NULL, 'asdas asdaf', NULL, 'hashsdh asdasd', NULL, 'md5', NULL, NULL, NULL, 'a', '2019-10-30 02:55:17', '2019-10-30 02:55:17', '1'),
(8, 'Eaque rem quidem ratione consequuntur temporibus. Accusantium nostrum labore fuga voluptatibus. Optio in non fuga alias ut nihil repellat. Assumenda est et rerum et ullam aspernatur nam.', NULL, 'Wakiman Luwar Wasita', NULL, 'Rahmat Sihotang M.TI.', NULL, 'Uchita Wulandari', NULL, 'Dacin Imam Permadi S.Sos', NULL, 'Raisa Hassanah S.Pd', NULL, 'Latif Wibisono', NULL, 'c', NULL, NULL, '1'),
(9, 'Quaerat aut asperiores voluptas corrupti. Quod voluptate consequuntur nihil hic explicabo ut sunt. Asperiores unde labore quis illo autem aliquam modi.', NULL, 'Vicky Syahrini Halimah', NULL, 'Asmuni Artanto Saragih', NULL, 'Titin Pertiwi', NULL, 'Daruna Firgantoro', NULL, 'Yuni Utami S.T.', NULL, 'Ian Permadi', NULL, 'e', NULL, NULL, '1'),
(10, 'Vero expedita corporis perferendis cumque est vero. Id iure voluptas a consequuntur. Alias sint culpa sint dignissimos beatae. Ab perspiciatis non molestiae omnis dolore et praesentium.', NULL, 'Citra Riyanti M.Kom.', NULL, 'Ghaliyati Anita Aryani S.H.', NULL, 'Pranawa Damu Prasetyo S.E.I', NULL, 'Mustofa Setiawan', NULL, 'Qori Wahyuni', NULL, 'Damar Estiawan Thamrin', NULL, 'f', NULL, NULL, '1'),
(11, 'Aliquam est quasi quia. Dicta reprehenderit neque velit molestias ratione. Aut nesciunt incidunt ut quidem voluptatibus adipisci.', NULL, 'Kambali Sihotang', NULL, 'Cahya Santoso', NULL, 'Uli Genta Mayasari S.Pt', NULL, 'Mahesa Situmorang', NULL, 'Endah Mayasari', NULL, 'Ibrahim Radika Waluyo', NULL, 'b', NULL, NULL, '1'),
(12, 'Et est quia quisquam. Aut dolor non quos voluptas est error vel doloribus. Ut esse dolorem sit tenetur et. Officiis molestiae aut ipsam ab dolores aut.', NULL, 'Teguh Purwadi Habibi S.E.', NULL, 'Salsabila Aisyah Aryani S.Psi', NULL, 'Saka Naradi Mangunsong', NULL, 'Azalea Laksita S.E.I', NULL, 'Prayitna Wacana', NULL, 'Belinda Widiastuti S.Psi', NULL, 'f', NULL, NULL, '1'),
(13, 'Qui cumque dolorem fugit rem. Illo quia ut quo ut. Ea numquam delectus fugit consectetur accusamus explicabo. Perspiciatis aut sit aut vero doloribus.', NULL, 'Jamalia Raina Aryani', NULL, 'Cager Lasmono Hidayanto', NULL, 'Vera Ulva Utami', NULL, 'Lega Samosir S.IP', NULL, 'Laksana Drajat Hakim', NULL, 'Jono Hidayat M.M.', NULL, 'f', NULL, NULL, '1'),
(14, 'Aut quis rerum sequi optio omnis dolores itaque esse. Distinctio vero est eum qui aut animi quod. Debitis eum a aperiam modi id inventore. Voluptatem vel aperiam vel iure.', NULL, 'Pranawa Rama Nugroho S.H.', NULL, 'Manah Kurniawan', NULL, 'Jasmani Pradipta S.Ked', NULL, 'Cakrabuana Wahyudin', NULL, 'Padmi Diah Widiastuti S.Pd', NULL, 'Dimaz Pradana', NULL, 'b', NULL, NULL, '1'),
(15, 'Eligendi officiis velit quia quo saepe nam. Tempora saepe odit aperiam nam et nihil. Hic mollitia sapiente aut ea reprehenderit. Vel culpa possimus temporibus est omnis.', NULL, 'Ratna Wahyuni', NULL, 'Puspa Uli Usamah', NULL, 'Raden Utama', NULL, 'Olivia Pudjiastuti', NULL, 'Karsa Vinsen Firgantoro S.Psi', NULL, 'Padmi Suartini', NULL, 'e', NULL, NULL, '1'),
(16, 'Sed non dignissimos quam qui consequatur at. Adipisci et laborum omnis aut voluptas inventore error. Facere aperiam accusamus quae voluptatem et nobis quibusdam.', NULL, 'Oliva Wijayanti', NULL, 'Uda Slamet Wibisono S.Sos', NULL, 'Agus Hamzah Halim', NULL, 'Candra Irsad Saputra S.Ked', NULL, 'Zalindra Wijayanti S.Kom', NULL, 'Ridwan Galur Hutagalung S.Ked', NULL, 'c', NULL, NULL, '1'),
(17, 'Animi enim illo deleniti et quia molestias magnam voluptates. Quaerat possimus at nisi nulla pariatur repudiandae. Et libero pariatur dolores nulla rerum. Atque dicta modi sapiente tempore.', NULL, 'Gandi Nashiruddin', NULL, 'Diana Rahayu', NULL, 'Pandu Dabukke', NULL, 'Sabrina Samiah Yuliarti', NULL, 'Embuh Aris Januar M.Kom.', NULL, 'Jasmin Sudiati', NULL, 'f', NULL, NULL, '1'),
(18, 'Ea laboriosam alias distinctio praesentium eligendi ducimus exercitationem. Excepturi in architecto nisi.', NULL, 'Mila Aryani', NULL, 'Nasab Putra', NULL, 'Vero Siregar', NULL, 'Zelda Gasti Prastuti', NULL, 'Pardi Ramadan', NULL, 'Kemal Simanjuntak', NULL, 'f', NULL, NULL, '1'),
(19, 'Qui tenetur sapiente dignissimos dolores debitis vitae. Adipisci aut perspiciatis id. Odio libero qui hic similique.', NULL, 'Tomi Sihombing', NULL, 'Endra Maryadi', NULL, 'Tari Oni Laksita', NULL, 'Yunita Kuswandari', NULL, 'Puti Oktaviani S.IP', NULL, 'Genta Laksita M.Kom.', NULL, 'e', NULL, NULL, '1'),
(20, 'Id perspiciatis fugit qui ut. Excepturi eum non non corrupti ut. Maiores totam consequatur adipisci enim. Cupiditate ex autem et quis dolorem repellat quasi.', NULL, 'Hesti Rahmawati M.Farm', NULL, 'Rahayu Hassanah M.Kom.', NULL, 'Clara Nasyiah', NULL, 'Cager Mahendra', NULL, 'Kasiran Simanjuntak', NULL, 'Cemani Firgantoro', NULL, 'a', NULL, NULL, '1'),
(21, 'Aliquid eum et magni et voluptas nisi nisi. Dignissimos et accusamus doloremque fugiat vel odit autem.', NULL, 'Garang Daru Ardianto S.Pd', NULL, 'Juli Suartini S.H.', NULL, 'Tari Pratiwi', NULL, 'Eja Karja Maryadi M.Farm', NULL, 'Daryani Marbun', NULL, 'Estiawan Utama S.Farm', NULL, 'e', NULL, NULL, '1'),
(22, 'Perspiciatis et amet soluta sint magnam. Et illo autem ullam est eveniet nemo. Distinctio eum illo accusamus.', NULL, 'Hasim Siregar S.E.', NULL, 'Endah Ciaobella Kuswandari', NULL, 'Teddy Latupono S.Ked', NULL, 'Najam Gangsar Thamrin', NULL, 'Gandewa Wijaya', NULL, 'Bagas Soleh Kuswoyo', NULL, 'b', NULL, NULL, '1'),
(23, 'Provident ullam culpa rem minus non. Architecto aliquam sed facere delectus. Et atque amet exercitationem beatae odit.', NULL, 'Kunthara Prasasta', NULL, 'Ina Laksita', NULL, 'Ella Laksita', NULL, 'Sakura Zulaikha Wulandari', NULL, 'Ira Astuti', NULL, 'Qori Melani S.Farm', NULL, 'd', NULL, NULL, '1'),
(24, 'Aut maxime accusantium laudantium. Ab et consequatur aliquid minima doloribus. Optio temporibus vero quidem vitae.', NULL, 'Garang Januar', NULL, 'Eka Yuliarti', NULL, 'Galur Pranowo', NULL, 'Among Nashiruddin S.H.', NULL, 'Harja Adriansyah S.E.I', NULL, 'Anastasia Lala Andriani', NULL, 'a', NULL, NULL, '1'),
(25, 'Perspiciatis iusto et consequatur. Officia architecto minus dolorem error et quam et. Et nihil illum molestias earum iure. Enim beatae voluptatem aut deleniti.', NULL, 'Samsul Samosir', NULL, 'Rachel Victoria Puspasari', NULL, 'Xanana Mahesa Budiman', NULL, 'Edward Sitorus', NULL, 'Harja Rusman Megantara S.E.', NULL, 'Silvia Yulianti S.Gz', NULL, 'b', NULL, NULL, '1'),
(26, 'Illum et omnis fuga placeat fugit. Iusto explicabo et animi et quibusdam ab nihil. Et sed qui voluptatem sunt voluptatem.', NULL, 'Dasa Mitra Wibowo', NULL, 'Yessi Puspa Wahyuni', NULL, 'Janet Kusmawati', NULL, 'Zaenab Mulyani M.TI.', NULL, 'Luluh Hakim', NULL, 'Halima Prastuti', NULL, 'a', NULL, NULL, '1'),
(27, 'Sapiente quia aut harum recusandae quas. Sunt nulla id dolore necessitatibus pariatur. Iure excepturi mollitia debitis facilis omnis ut.', NULL, 'Talia Puspasari S.Psi', NULL, 'Rahmi Julia Usamah S.Gz', NULL, 'Cahyadi Hadi Sihotang', NULL, 'Lalita Agustina', NULL, 'Eko Hidayat', NULL, 'Bakidin Kurniawan', NULL, 'd', NULL, NULL, '1'),
(28, NULL, NULL, 'Kpg. Bayan No. 297, Solok 79112, KalBar', NULL, 'Kpg. Baranang Siang Indah No. 498, Prabumulih 96628, Bengkulu', NULL, 'Dk. Lada No. 123, Prabumulih 25419, Lampung', NULL, 'Jr. Labu No. 834, Bandung 78998, Gorontalo', NULL, 'Ds. B.Agam Dlm No. 337, Bengkulu 30367, Lampung', NULL, NULL, NULL, 'e', NULL, NULL, '2'),
(29, NULL, NULL, 'Gg. Gambang No. 601, Ternate 38316, Aceh', NULL, 'Ki. Reksoninten No. 257, Banjarmasin 70741, DIY', NULL, 'Psr. Bara Tambar No. 987, Solok 19153, DKI', NULL, 'Jr. Tentara Pelajar No. 799, Surabaya 89695, Bengkulu', NULL, 'Dk. Monginsidi No. 648, Makassar 21411, Maluku', NULL, NULL, NULL, 'a', NULL, NULL, '2'),
(30, NULL, NULL, 'Ds. Bagis Utama No. 951, Yogyakarta 87639, SulSel', NULL, 'Kpg. Babadak No. 303, Kediri 68847, Riau', NULL, 'Psr. Jakarta No. 536, Banjarbaru 83828, SulUt', NULL, 'Dk. Jamika No. 787, Palopo 86314, KalTeng', NULL, 'Kpg. Rajiman No. 866, Tangerang 63859, NTB', NULL, NULL, NULL, 'd', NULL, NULL, '2'),
(31, NULL, NULL, 'Gg. Bah Jaya No. 508, Serang 28749, NTB', NULL, 'Kpg. Elang No. 462, Bau-Bau 80161, NTB', NULL, 'Jr. Ters. Kiaracondong No. 233, Kotamobagu 24223, MalUt', NULL, 'Gg. Sampangan No. 660, Gorontalo 51178, KalTim', NULL, 'Ds. Jend. Sudirman No. 843, Palu 60793, Lampung', NULL, NULL, NULL, 'c', NULL, NULL, '2'),
(32, NULL, NULL, 'Kpg. Jayawijaya No. 646, Tanjung Pinang 85697, Bali', NULL, 'Ds. Bappenas No. 371, Dumai 67766, Bengkulu', NULL, 'Kpg. Eka No. 704, Bekasi 36232, JaTim', NULL, 'Psr. BKR No. 384, Subulussalam 28809, KalUt', NULL, 'Jr. Setiabudhi No. 340, Blitar 85772, KalUt', NULL, NULL, NULL, 'd', NULL, NULL, '2'),
(33, NULL, NULL, 'Psr. Abdullah No. 265, Subulussalam 86213, Aceh', NULL, 'Jr. Barasak No. 59, Surakarta 50708, DKI', NULL, 'Jln. Madiun No. 491, Lubuklinggau 24216, NTT', NULL, 'Psr. Supomo No. 251, Surakarta 49985, Papua', NULL, 'Jr. Jend. Sudirman No. 646, Subulussalam 18449, SumUt', NULL, NULL, NULL, 'b', NULL, NULL, '2'),
(34, NULL, NULL, 'Jln. Tambak No. 399, Tual 34403, Jambi', NULL, 'Ki. Bass No. 226, Cilegon 25477, SumUt', NULL, 'Ki. Baranang No. 362, Blitar 91231, JaBar', NULL, 'Dk. Adisumarmo No. 471, Bitung 26262, Aceh', NULL, 'Jln. Juanda No. 39, Tasikmalaya 65501, SumUt', NULL, NULL, NULL, 'e', NULL, NULL, '2'),
(35, NULL, NULL, 'Gg. Elang No. 254, Padangsidempuan 64324, PapBar', NULL, 'Jr. Bass No. 864, Gorontalo 67799, KalTeng', NULL, 'Kpg. Sudirman No. 249, Blitar 54302, Papua', NULL, 'Gg. Bahagia  No. 69, Bima 47307, SulTeng', NULL, 'Gg. Wahid No. 343, Jambi 83583, Jambi', NULL, NULL, NULL, 'a', NULL, NULL, '2'),
(36, NULL, NULL, 'Ds. Bank Dagang Negara No. 267, Bima 21108, JaTeng', NULL, 'Jln. Bahagia No. 936, Palu 16450, BaBel', NULL, 'Dk. Soekarno Hatta No. 623, Sawahlunto 60820, Bali', NULL, 'Gg. Sutami No. 601, Banjarbaru 21057, Lampung', NULL, 'Dk. Padang No. 375, Tanjungbalai 89733, PapBar', NULL, NULL, NULL, 'b', NULL, NULL, '2'),
(37, NULL, NULL, 'Jr. S. Parman No. 84, Administrasi Jakarta Timur 90096, SumUt', NULL, 'Ki. Abdul Rahmat No. 454, Bekasi 20752, Maluku', NULL, 'Psr. Cut Nyak Dien No. 800, Gorontalo 29055, SulTra', NULL, 'Jln. Basuki No. 948, Bukittinggi 39970, MalUt', NULL, 'Ds. Urip Sumoharjo No. 274, Medan 71079, SulBar', NULL, NULL, NULL, 'a', NULL, NULL, '2'),
(38, NULL, NULL, 'Jln. Tambun No. 736, Bau-Bau 17926, Gorontalo', NULL, 'Jln. Bayan No. 320, Batam 92132, SumSel', NULL, 'Ds. Zamrud No. 399, Sawahlunto 37994, Bengkulu', NULL, 'Ki. Moch. Yamin No. 729, Prabumulih 90919, BaBel', NULL, 'Jln. Bappenas No. 540, Semarang 33531, JaBar', NULL, NULL, NULL, 'a', NULL, NULL, '2'),
(39, NULL, NULL, 'Gg. Abdul Rahmat No. 268, Balikpapan 45277, PapBar', NULL, 'Jln. Cihampelas No. 523, Langsa 64830, Jambi', NULL, 'Kpg. Sutarjo No. 295, Bukittinggi 27879, Gorontalo', NULL, 'Jln. Gegerkalong Hilir No. 876, Pagar Alam 13506, DIY', NULL, 'Ds. Bazuka Raya No. 406, Jambi 52294, Aceh', NULL, NULL, NULL, 'c', NULL, NULL, '2'),
(40, NULL, NULL, 'Jln. Dr. Junjunan No. 737, Gorontalo 68825, NTB', NULL, 'Jln. Katamso No. 331, Pagar Alam 40054, Papua', NULL, 'Dk. Tambun No. 996, Bogor 28345, Jambi', NULL, 'Ds. Pelajar Pejuang 45 No. 817, Cilegon 91358, BaBel', NULL, 'Ds. Lada No. 846, Ambon 61603, DKI', NULL, NULL, NULL, 'd', NULL, NULL, '2'),
(41, NULL, NULL, 'Jln. Dago No. 295, Kupang 91622, PapBar', NULL, 'Jln. Rajawali Timur No. 233, Cilegon 15085, DKI', NULL, 'Jln. Taman No. 819, Padang 36361, SulSel', NULL, 'Ki. Baja No. 57, Palangka Raya 26772, Bali', NULL, 'Psr. Baja No. 722, Blitar 49120, KalBar', NULL, NULL, NULL, 'e', NULL, NULL, '2'),
(42, NULL, NULL, 'Kpg. Kyai Gede No. 12, Bengkulu 41840, Bengkulu', NULL, 'Gg. Ujung No. 303, Pontianak 74121, DKI', NULL, 'Dk. Jagakarsa No. 380, Dumai 24288, JaTeng', NULL, 'Ds. Elang No. 210, Administrasi Jakarta Utara 17147, PapBar', NULL, 'Psr. Sampangan No. 502, Parepare 91223, Papua', NULL, NULL, NULL, 'd', NULL, NULL, '2'),
(43, 'Dolorem atque quisquam voluptatibus explicabo. Omnis ipsam aut voluptatem porro.', NULL, 'Kpg. Padang No. 403, Binjai 95694, SumUt', NULL, 'Gg. Sumpah Pemuda No. 881, Bau-Bau 80681, JaBar', NULL, 'Psr. Camar No. 180, Palembang 32471, JaTeng', NULL, 'Jr. Camar No. 329, Palembang 16737, BaBel', NULL, 'Ds. Bazuka Raya No. 128, Serang 65155, SulTeng', NULL, 'Ds. Ters. Jakarta No. 849, Tasikmalaya 10839, Banten', NULL, 'f', NULL, NULL, '3'),
(44, 'Et sit suscipit et autem aut. Quos veritatis quod quas rerum.', NULL, 'Dk. Bass No. 7, Gunungsitoli 73607, JaTeng', NULL, 'Jr. Cikutra Timur No. 712, Administrasi Jakarta Pusat 12404, SumSel', NULL, 'Dk. Yap Tjwan Bing No. 178, Banda Aceh 27230, Lampung', NULL, 'Psr. Bak Mandi No. 848, Bandar Lampung 37412, Papua', NULL, 'Gg. Padang No. 765, Pekanbaru 80986, Jambi', NULL, 'Psr. Pasir Koja No. 518, Banjar 43695, KalUt', NULL, 'd', NULL, NULL, '3'),
(45, 'Sequi totam rerum in rerum ut dolor. Commodi aut maiores laboriosam ut vero natus ad harum.', NULL, 'Jr. Bah Jaya No. 570, Denpasar 14531, Jambi', NULL, 'Jln. B.Agam 1 No. 614, Bima 56443, SumSel', NULL, 'Kpg. Baranangsiang No. 379, Tasikmalaya 87893, SulBar', NULL, 'Jln. Jambu No. 384, Dumai 36292, Gorontalo', NULL, 'Jr. Bahagia No. 466, Kupang 93803, JaBar', NULL, 'Ds. Fajar No. 716, Depok 23530, Lampung', NULL, 'b', NULL, NULL, '3'),
(46, 'Placeat illum qui vero. At voluptatem in voluptas quos dolor. Possimus officia optio recusandae ut.', NULL, 'Dk. Suprapto No. 301, Pariaman 78109, Bali', NULL, 'Kpg. Lada No. 860, Batu 42159, NTT', NULL, 'Jr. Ahmad Dahlan No. 851, Pekanbaru 90209, KalSel', NULL, 'Gg. Orang No. 741, Tanjungbalai 37313, Banten', NULL, 'Jr. Aceh No. 123, Dumai 53780, Bengkulu', NULL, 'Ki. Wahid Hasyim No. 804, Pekanbaru 99310, SumSel', NULL, 'b', NULL, NULL, '3'),
(47, 'Vero et ea et ut vero non. Cumque sint libero iste sit. Tenetur vero et eligendi est et.', NULL, 'Psr. Bakit  No. 178, Tanjung Pinang 26088, SumBar', NULL, 'Gg. Suprapto No. 765, Metro 46334, SulUt', NULL, 'Ds. Bakaru No. 815, Malang 34321, DIY', NULL, 'Dk. Bakin No. 9, Administrasi Jakarta Pusat 29854, Riau', NULL, 'Jln. Veteran No. 670, Padang 74649, Jambi', NULL, 'Kpg. Radio No. 70, Banda Aceh 37201, NTB', NULL, 'a', NULL, NULL, '3'),
(48, 'Quas veniam occaecati laudantium voluptatibus aut ratione. Occaecati corporis neque molestiae non.', NULL, 'Psr. Babadak No. 905, Tegal 16985, KalBar', NULL, 'Gg. Barat No. 494, Cilegon 81819, JaTeng', NULL, 'Psr. Sentot Alibasa No. 282, Bukittinggi 48855, KalBar', NULL, 'Jln. Perintis Kemerdekaan No. 132, Bukittinggi 73989, KalTeng', NULL, 'Kpg. Cikutra Barat No. 44, Yogyakarta 82896, Gorontalo', NULL, 'Jr. Baja No. 926, Banjarmasin 57393, JaTeng', NULL, 'c', NULL, NULL, '3'),
(49, 'Tempora ab nihil sed sed voluptas. Asperiores vel quis ratione eveniet.', NULL, 'Jr. Jamika No. 580, Sukabumi 29887, SulSel', NULL, 'Gg. Banal No. 602, Lubuklinggau 99979, JaTeng', NULL, 'Ki. Baung No. 462, Cilegon 21733, SulUt', NULL, 'Ki. Yap Tjwan Bing No. 21, Ambon 96504, Jambi', NULL, 'Psr. Ters. Pasir Koja No. 125, Tanjungbalai 17671, Banten', NULL, 'Gg. Nakula No. 714, Palu 90808, JaTim', NULL, 'c', NULL, NULL, '3'),
(50, 'Omnis et excepturi sed adipisci. Odio rerum unde soluta. Quia molestiae aut repellendus cumque.', NULL, 'Ki. Bara No. 495, Bitung 65879, SumUt', NULL, 'Ds. Kusmanto No. 263, Pekanbaru 69065, BaBel', NULL, 'Jln. Cemara No. 743, Sukabumi 83586, PapBar', NULL, 'Jln. Daan No. 678, Kediri 94863, NTT', NULL, 'Jr. Gardujati No. 900, Surakarta 47550, SulBar', NULL, 'Gg. Lembong No. 820, Pontianak 72533, JaTeng', NULL, 'b', NULL, NULL, '3'),
(51, 'Facere perspiciatis voluptas sit optio facere quas nulla. Reprehenderit iusto et et ea eos.', NULL, 'Kpg. Yoga No. 612, Pangkal Pinang 91322, KalTeng', NULL, 'Jln. Mahakam No. 577, Administrasi Jakarta Timur 11529, KepR', NULL, 'Jln. Mahakam No. 756, Payakumbuh 69545, JaTeng', NULL, 'Ds. Untung Suropati No. 390, Kotamobagu 90665, SumUt', NULL, 'Gg. Ciwastra No. 842, Cimahi 34466, Maluku', NULL, 'Jr. Siliwangi No. 140, Bukittinggi 24727, SumBar', NULL, 'e', NULL, NULL, '3'),
(52, 'Consequatur suscipit et et rerum odit. Quia ad corporis cupiditate ut.', NULL, 'Ki. K.H. Maskur No. 626, Prabumulih 60802, Maluku', NULL, 'Jln. Haji No. 512, Cimahi 54899, NTT', NULL, 'Kpg. Basuki Rahmat  No. 871, Banjar 85804, SulSel', NULL, 'Jr. Kiaracondong No. 342, Probolinggo 79553, JaTeng', NULL, 'Ds. Kyai Mojo No. 394, Balikpapan 31647, Aceh', NULL, 'Jr. Baranang Siang No. 553, Manado 58338, SulSel', NULL, 'd', NULL, NULL, '3'),
(53, 'Est porro omnis eligendi ut tempora neque. Neque pariatur ipsam fugiat corporis repellendus.', NULL, 'Ds. Gotong Royong No. 443, Tanjungbalai 96489, KepR', NULL, 'Kpg. Bacang No. 945, Pariaman 82566, DIY', NULL, 'Jr. Babadan No. 952, Langsa 38266, BaBel', NULL, 'Kpg. Bakti No. 628, Padangsidempuan 65699, JaTeng', NULL, 'Ds. Banda No. 926, Tarakan 87213, SumSel', NULL, 'Jln. Ir. H. Juanda No. 162, Bengkulu 99341, MalUt', NULL, 'c', NULL, NULL, '3'),
(54, 'Libero rerum harum ducimus. Incidunt minus qui impedit tempore eligendi rem.', NULL, 'Psr. Salatiga No. 478, Tarakan 13728, BaBel', NULL, 'Psr. Fajar No. 998, Cirebon 57188, MalUt', NULL, 'Psr. Kali No. 875, Tangerang Selatan 77139, SumUt', NULL, 'Kpg. Cikutra Timur No. 984, Bima 33277, BaBel', NULL, 'Psr. Rajawali Timur No. 459, Manado 79610, DKI', NULL, 'Dk. Baranangsiang No. 436, Sabang 64411, DIY', NULL, 'a', NULL, NULL, '3'),
(55, 'In possimus aspernatur magni. Ducimus voluptas quo quae eius aut nisi. Soluta soluta optio iste.', NULL, 'Jln. Dr. Junjunan No. 199, Pasuruan 81447, DIY', NULL, 'Gg. Bakti No. 652, Palangka Raya 12379, SulTra', NULL, 'Ds. Gatot Subroto No. 358, Parepare 29213, NTT', NULL, 'Jr. Yogyakarta No. 304, Administrasi Jakarta Selatan 76742, SulBar', NULL, 'Jln. Jayawijaya No. 66, Parepare 61179, MalUt', NULL, 'Psr. Raya Setiabudhi No. 401, Administrasi Jakarta Selatan 93604, SumBar', NULL, 'd', NULL, NULL, '3'),
(56, 'Iste qui dolores est. Esse ea id et quo. Voluptatem ut sint dolorem itaque accusamus.', NULL, 'Gg. Ikan No. 350, Gunungsitoli 25530, Aceh', NULL, 'Jln. R.M. Said No. 899, Depok 14101, SumSel', NULL, 'Psr. Mulyadi No. 965, Probolinggo 64530, KalTeng', NULL, 'Psr. Sentot Alibasa No. 971, Pekalongan 88769, KalTeng', NULL, 'Ki. Agus Salim No. 237, Tual 21867, NTT', NULL, 'Psr. Setia Budi No. 289, Tasikmalaya 10608, DIY', NULL, 'a', NULL, NULL, '3'),
(57, 'Error et hic et. Modi autem id sit aperiam.', NULL, 'Kpg. Lumban Tobing No. 993, Tanjung Pinang 68988, SulTeng', NULL, 'Psr. Baranang No. 180, Gorontalo 25043, SulSel', NULL, 'Kpg. Krakatau No. 407, Padangsidempuan 60740, NTB', NULL, 'Psr. Mulyadi No. 31, Administrasi Jakarta Selatan 83113, Gorontalo', NULL, 'Jln. Baiduri No. 364, Cimahi 92909, Jambi', NULL, 'Jln. Sam Ratulangi No. 245, Pematangsiantar 16795, DIY', NULL, 'c', NULL, NULL, '3'),
(58, 'Aut aperiam quasi non. Inventore voluptatem aut quam recusandae aut a sit ducimus. Quis dolores ut officia deserunt.', NULL, 'Dono Permadi', NULL, 'Wulan Uli Farida', NULL, 'Dimas Kala Simbolon M.Ak', NULL, 'Ajimin Prasasta', NULL, 'Siska Natalia Safitri', NULL, NULL, NULL, 'b', NULL, NULL, '4'),
(59, 'Maxime aut ut aliquid quasi beatae. Sit nostrum voluptas aspernatur consequatur quasi cumque dolor. Sunt natus id tenetur neque ex.', NULL, 'Ellis Ajeng Maryati', NULL, 'Daniswara Firmansyah M.TI.', NULL, 'Maimunah Bella Andriani', NULL, 'Endra Sirait', NULL, 'Aswani Kasim Simanjuntak', NULL, NULL, NULL, 'b', NULL, NULL, '4'),
(60, 'Hic ut vel molestiae mollitia saepe nobis. Maxime deserunt et odit est ea. Consequatur in aperiam nesciunt.', NULL, 'Ciaobella Purnawati S.I.Kom', NULL, 'Amalia Wulandari', NULL, 'Patricia Malika Wastuti', NULL, 'Patricia Ika Zulaika', NULL, 'Syahrini Hilda Pudjiastuti', NULL, NULL, NULL, 'b', NULL, NULL, '4'),
(61, 'Repudiandae nesciunt mollitia rerum voluptas omnis. Aliquam fugit rerum et autem. Mollitia laudantium velit quas.', NULL, 'Harsaya Nugroho', NULL, 'Novi Rahmi Puspasari', NULL, 'Gatra Januar S.Kom', NULL, 'Nalar Budiman', NULL, 'Zalindra Wijayanti', NULL, NULL, NULL, 'c', NULL, NULL, '4'),
(62, 'Hic dolorem necessitatibus eos iusto dolorem perferendis facere. Ea odio recusandae fugit doloribus quam id doloribus vitae.', NULL, 'Wadi Widodo', NULL, 'Hairyanto Cakrawala Nainggolan', NULL, 'Opung Estiono Siregar', NULL, 'Zelaya Diah Purnawati', NULL, 'Prakosa Damanik', NULL, NULL, NULL, 'b', NULL, NULL, '4'),
(63, 'Cumque cum nobis aperiam sit dolor. Magni reiciendis eaque architecto labore iste nihil vero adipisci.', NULL, 'Naradi Hidayanto S.I.Kom', NULL, 'Lamar Maheswara', NULL, 'Lembah Waskita M.M.', NULL, 'Najwa Aryani', NULL, 'Irwan Wibowo', NULL, NULL, NULL, 'e', NULL, NULL, '4'),
(64, 'Quia mollitia id qui sed. In commodi et dolor. Eius quis aut cupiditate repudiandae laudantium quos. Modi rem aut id sed.', NULL, 'Nurul Oni Nurdiyanti', NULL, 'Mulyanto Manah Hakim S.E.', NULL, 'Mala Faizah Agustina S.Sos', NULL, 'Wardaya Nugroho', NULL, 'Novi Lintang Agustina S.Farm', NULL, NULL, NULL, 'e', NULL, NULL, '4'),
(65, 'Occaecati hic deleniti et aut. Eum nobis modi ut non doloremque.', NULL, 'Jati Candrakanta Sitompul M.Kom.', NULL, 'Catur Dabukke', NULL, 'Ajimat Pradana', NULL, 'Galiono Balapati Mangunsong', NULL, 'Gambira Mansur', NULL, NULL, NULL, 'b', NULL, NULL, '4'),
(66, 'Asperiores excepturi qui sunt omnis dolorum. Ipsam dolorum commodi qui non. Quia expedita eligendi molestiae laborum.', NULL, 'Dwi Mangunsong', NULL, 'Taswir Halim S.IP', NULL, 'Ozy Marpaung', NULL, 'Bakidin Mulyanto Hutagalung S.Gz', NULL, 'Manah Maryadi', NULL, NULL, NULL, 'b', NULL, NULL, '4'),
(67, 'Incidunt distinctio non occaecati quo et impedit. Corrupti dicta ipsa qui iure nihil omnis. Nobis assumenda iusto debitis veniam.', NULL, 'Vicky Sudiati S.Pd', NULL, 'Jaswadi Januar', NULL, 'Hasna Novitasari', NULL, 'Edi Mansur', NULL, 'Lintang Ifa Purwanti S.Kom', NULL, NULL, NULL, 'a', NULL, NULL, '4');

-- --------------------------------------------------------

--
-- Struktur dari tabel `soal_testiq_jenis`
--

CREATE TABLE `soal_testiq_jenis` (
  `id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_soal` int(11) NOT NULL,
  `waktu` int(11) NOT NULL COMMENT 'waktu pengerjaan, mis. 3 menit',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `soal_testiq_jenis`
--

INSERT INTO `soal_testiq_jenis` (`id`, `name`, `jumlah_soal`, `waktu`, `created_at`, `updated_at`) VALUES
(1, 'Series', 12, 180, NULL, NULL),
(2, 'Clasification', 14, 240, NULL, NULL),
(3, 'Matrices', 12, 180, NULL, NULL),
(4, 'Conditions / Topology', 8, 150, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `soal_testiq_rumus`
--

CREATE TABLE `soal_testiq_rumus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `benar` int(11) NOT NULL,
  `nilai` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `soal_testiq_rumus`
--

INSERT INTO `soal_testiq_rumus` (`id`, `benar`, `nilai`, `created_at`, `updated_at`) VALUES
(1, 0, 38, NULL, NULL),
(2, 1, 40, NULL, NULL),
(3, 2, 43, NULL, NULL),
(4, 3, 45, NULL, NULL),
(5, 4, 47, NULL, NULL),
(6, 5, 49, NULL, NULL),
(7, 6, 52, NULL, NULL),
(8, 7, 55, NULL, NULL),
(9, 8, 57, NULL, NULL),
(10, 9, 60, NULL, NULL),
(11, 10, 63, NULL, NULL),
(12, 11, 67, NULL, NULL),
(13, 12, 70, NULL, NULL),
(14, 13, 72, NULL, NULL),
(15, 14, 75, NULL, NULL),
(16, 15, 78, NULL, NULL),
(17, 16, 81, NULL, NULL),
(18, 17, 85, NULL, NULL),
(19, 18, 88, NULL, NULL),
(20, 19, 91, NULL, NULL),
(21, 20, 94, NULL, NULL),
(22, 21, 96, NULL, NULL),
(23, 22, 100, NULL, NULL),
(24, 23, 103, NULL, NULL),
(25, 24, 106, NULL, NULL),
(26, 25, 109, NULL, NULL),
(27, 26, 113, NULL, NULL),
(28, 27, 116, NULL, NULL),
(29, 28, 119, NULL, NULL),
(30, 29, 121, NULL, NULL),
(31, 30, 124, NULL, NULL),
(32, 31, 128, NULL, NULL),
(33, 32, 131, NULL, NULL),
(34, 33, 133, NULL, NULL),
(35, 34, 137, NULL, NULL),
(36, 35, 140, NULL, NULL),
(37, 36, 142, NULL, NULL),
(38, 37, 145, NULL, NULL),
(39, 38, 149, NULL, NULL),
(40, 39, 152, NULL, NULL),
(41, 40, 155, NULL, NULL),
(42, 41, 157, NULL, NULL),
(43, 42, 161, NULL, NULL),
(44, 43, 165, NULL, NULL),
(45, 44, 167, NULL, NULL),
(46, 45, 169, NULL, NULL),
(47, 46, 173, NULL, NULL),
(48, 47, 176, NULL, NULL),
(49, 48, 179, NULL, NULL),
(50, 49, 183, NULL, NULL),
(51, 50, 183, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `soal_testiq_temps`
--

CREATE TABLE `soal_testiq_temps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `userujian_id` int(11) NOT NULL,
  `soaltestiq_id` int(11) NOT NULL,
  `jawaban` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis` enum('1','2','3','4') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ujian_jeniss`
--

CREATE TABLE `ujian_jeniss` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `ujian_jeniss`
--

INSERT INTO `ujian_jeniss` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Test Ketik', '1', NULL, NULL),
(2, 'Test Administrasi (Excel)', '1', NULL, NULL),
(3, 'Test IQ', '1', NULL, NULL),
(4, 'Test Papi Kostick', '1', NULL, NULL),
(5, 'Test DISC', '1', NULL, NULL),
(6, 'Test Deret Angka (Kraepelin)', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '0: superadmin, 1: admin, 2: employer, 3: job seeker',
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0:non-aktif, 1: aktif',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `remember_token`, `photo`, `api_token`, `token`, `role`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Mirza Purnandi', 'mirza', 'mirza@m3codes.com', '2019-10-24 19:51:59', '$2y$10$j3XSTrHeC.ydpGrJT8gtV.iDA8zNIXf2Dt4h4ZV/kO8Uyt4J2ura6', NULL, NULL, 'Yxy7GxZRc9WiBIrrY4P1piSAp20GI2rXlLaZLISM', NULL, '0', '1', '2019-10-24 19:52:00', '2019-11-19 23:25:26'),
(8, 'Admin Alfamart', 'Ey3uADiC3g', 'mirza@emc.group', NULL, '$2y$10$0T/HizGiiazAUjb2bLmuIOo.1HI3xl6pWD3rUQSZlA26HDd24P4yW', NULL, NULL, 'MIoKiQ41Dopqsduzml1wFh20SF9XN9FIRn12xYci', '53a0af280fbb49b493661dad1ff8f614', '3', '1', '2019-10-24 23:53:59', '2019-11-19 23:18:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_ujians`
--

CREATE TABLE `user_ujians` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ujianjenis_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `benar` int(11) DEFAULT NULL,
  `salah` int(11) DEFAULT NULL,
  `status` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `score` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `list_soal` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `waktu_mulai` datetime DEFAULT NULL,
  `waktu_akhir` datetime DEFAULT NULL,
  `ujian_mulai` datetime DEFAULT NULL,
  `ujian_akhir` datetime DEFAULT NULL,
  `ujian_akhir2` datetime DEFAULT NULL,
  `ujian_akhir3` datetime DEFAULT NULL,
  `ujian_akhir4` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `user_ujians`
--

INSERT INTO `user_ujians` (`id`, `user_id`, `ujianjenis_id`, `jumlah`, `benar`, `salah`, `status`, `score`, `list_soal`, `created_at`, `updated_at`, `waktu_mulai`, `waktu_akhir`, `ujian_mulai`, `ujian_akhir`, `ujian_akhir2`, `ujian_akhir3`, `ujian_akhir4`) VALUES
(1, '8', 3, 0, 11, 35, '2', '67', '[{\"id\":1,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":1,\"jawaban\":\"a\",\"jenis\":\"1\",\"created_at\":\"2019-11-20 06:19:26\",\"updated_at\":\"2019-11-20 06:19:51\"},{\"id\":2,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":3,\"jawaban\":\"a\",\"jenis\":\"1\",\"created_at\":\"2019-11-20 06:19:27\",\"updated_at\":\"2019-11-20 06:19:52\"},{\"id\":3,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":4,\"jawaban\":\"a\",\"jenis\":\"1\",\"created_at\":\"2019-11-20 06:19:27\",\"updated_at\":\"2019-11-20 06:19:53\"},{\"id\":4,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":5,\"jawaban\":\"a\",\"jenis\":\"1\",\"created_at\":\"2019-11-20 06:19:27\",\"updated_at\":\"2019-11-20 06:19:55\"},{\"id\":5,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":7,\"jawaban\":\"a\",\"jenis\":\"1\",\"created_at\":\"2019-11-20 06:19:27\",\"updated_at\":\"2019-11-20 06:19:56\"},{\"id\":6,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":8,\"jawaban\":\"a\",\"jenis\":\"1\",\"created_at\":\"2019-11-20 06:19:27\",\"updated_at\":\"2019-11-20 06:20:01\"},{\"id\":7,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":9,\"jawaban\":\"a\",\"jenis\":\"1\",\"created_at\":\"2019-11-20 06:19:27\",\"updated_at\":\"2019-11-20 06:20:02\"},{\"id\":8,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":10,\"jawaban\":\"a\",\"jenis\":\"1\",\"created_at\":\"2019-11-20 06:19:27\",\"updated_at\":\"2019-11-20 06:20:03\"},{\"id\":9,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":11,\"jawaban\":\"a\",\"jenis\":\"1\",\"created_at\":\"2019-11-20 06:19:27\",\"updated_at\":\"2019-11-20 06:20:05\"},{\"id\":10,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":12,\"jawaban\":\"a\",\"jenis\":\"1\",\"created_at\":\"2019-11-20 06:19:27\",\"updated_at\":\"2019-11-20 06:20:06\"},{\"id\":11,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":13,\"jawaban\":\"a\",\"jenis\":\"1\",\"created_at\":\"2019-11-20 06:19:27\",\"updated_at\":\"2019-11-20 06:20:07\"},{\"id\":12,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":14,\"jawaban\":\"a\",\"jenis\":\"1\",\"created_at\":\"2019-11-20 06:19:27\",\"updated_at\":\"2019-11-20 06:20:09\"},{\"id\":13,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":28,\"jawaban\":\"b\",\"jenis\":\"2\",\"created_at\":\"2019-11-20 06:20:40\",\"updated_at\":\"2019-11-20 06:20:53\"},{\"id\":14,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":29,\"jawaban\":\"b\",\"jenis\":\"2\",\"created_at\":\"2019-11-20 06:20:40\",\"updated_at\":\"2019-11-20 06:20:55\"},{\"id\":15,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":30,\"jawaban\":\"b\",\"jenis\":\"2\",\"created_at\":\"2019-11-20 06:20:40\",\"updated_at\":\"2019-11-20 06:20:56\"},{\"id\":16,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":31,\"jawaban\":\"b\",\"jenis\":\"2\",\"created_at\":\"2019-11-20 06:20:40\",\"updated_at\":\"2019-11-20 06:20:57\"},{\"id\":17,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":32,\"jawaban\":\"b\",\"jenis\":\"2\",\"created_at\":\"2019-11-20 06:20:40\",\"updated_at\":\"2019-11-20 06:20:58\"},{\"id\":18,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":33,\"jawaban\":\"b\",\"jenis\":\"2\",\"created_at\":\"2019-11-20 06:20:40\",\"updated_at\":\"2019-11-20 06:20:59\"},{\"id\":19,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":34,\"jawaban\":\"b\",\"jenis\":\"2\",\"created_at\":\"2019-11-20 06:20:40\",\"updated_at\":\"2019-11-20 06:21:00\"},{\"id\":20,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":35,\"jawaban\":\"b\",\"jenis\":\"2\",\"created_at\":\"2019-11-20 06:20:40\",\"updated_at\":\"2019-11-20 06:21:02\"},{\"id\":21,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":36,\"jawaban\":\"b\",\"jenis\":\"2\",\"created_at\":\"2019-11-20 06:20:40\",\"updated_at\":\"2019-11-20 06:21:03\"},{\"id\":22,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":37,\"jawaban\":\"b\",\"jenis\":\"2\",\"created_at\":\"2019-11-20 06:20:40\",\"updated_at\":\"2019-11-20 06:21:04\"},{\"id\":23,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":38,\"jawaban\":\"b\",\"jenis\":\"2\",\"created_at\":\"2019-11-20 06:20:40\",\"updated_at\":\"2019-11-20 06:21:06\"},{\"id\":24,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":39,\"jawaban\":\"b\",\"jenis\":\"2\",\"created_at\":\"2019-11-20 06:20:40\",\"updated_at\":\"2019-11-20 06:21:08\"},{\"id\":25,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":40,\"jawaban\":\"b\",\"jenis\":\"2\",\"created_at\":\"2019-11-20 06:20:40\",\"updated_at\":\"2019-11-20 06:21:09\"},{\"id\":26,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":41,\"jawaban\":null,\"jenis\":\"2\",\"created_at\":\"2019-11-20 06:20:41\",\"updated_at\":\"2019-11-20 06:20:41\"},{\"id\":27,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":43,\"jawaban\":\"d\",\"jenis\":\"3\",\"created_at\":\"2019-11-20 06:21:23\",\"updated_at\":\"2019-11-20 06:21:27\"},{\"id\":28,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":44,\"jawaban\":\"d\",\"jenis\":\"3\",\"created_at\":\"2019-11-20 06:21:23\",\"updated_at\":\"2019-11-20 06:22:15\"},{\"id\":29,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":45,\"jawaban\":\"d\",\"jenis\":\"3\",\"created_at\":\"2019-11-20 06:21:23\",\"updated_at\":\"2019-11-20 06:22:22\"},{\"id\":30,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":46,\"jawaban\":\"d\",\"jenis\":\"3\",\"created_at\":\"2019-11-20 06:21:23\",\"updated_at\":\"2019-11-20 06:22:26\"},{\"id\":31,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":47,\"jawaban\":\"d\",\"jenis\":\"3\",\"created_at\":\"2019-11-20 06:21:23\",\"updated_at\":\"2019-11-20 06:22:27\"},{\"id\":32,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":48,\"jawaban\":\"d\",\"jenis\":\"3\",\"created_at\":\"2019-11-20 06:21:23\",\"updated_at\":\"2019-11-20 06:22:28\"},{\"id\":33,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":49,\"jawaban\":\"d\",\"jenis\":\"3\",\"created_at\":\"2019-11-20 06:21:23\",\"updated_at\":\"2019-11-20 06:22:30\"},{\"id\":34,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":50,\"jawaban\":\"d\",\"jenis\":\"3\",\"created_at\":\"2019-11-20 06:21:23\",\"updated_at\":\"2019-11-20 06:22:32\"},{\"id\":35,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":51,\"jawaban\":\"d\",\"jenis\":\"3\",\"created_at\":\"2019-11-20 06:21:23\",\"updated_at\":\"2019-11-20 06:22:33\"},{\"id\":36,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":52,\"jawaban\":\"d\",\"jenis\":\"3\",\"created_at\":\"2019-11-20 06:21:23\",\"updated_at\":\"2019-11-20 06:22:35\"},{\"id\":37,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":53,\"jawaban\":\"d\",\"jenis\":\"3\",\"created_at\":\"2019-11-20 06:21:23\",\"updated_at\":\"2019-11-20 06:22:36\"},{\"id\":38,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":54,\"jawaban\":\"d\",\"jenis\":\"3\",\"created_at\":\"2019-11-20 06:21:23\",\"updated_at\":\"2019-11-20 06:22:38\"},{\"id\":39,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":58,\"jawaban\":\"e\",\"jenis\":\"4\",\"created_at\":\"2019-11-20 06:24:41\",\"updated_at\":\"2019-11-20 06:24:45\"},{\"id\":40,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":59,\"jawaban\":\"e\",\"jenis\":\"4\",\"created_at\":\"2019-11-20 06:24:41\",\"updated_at\":\"2019-11-20 06:24:46\"},{\"id\":41,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":60,\"jawaban\":\"e\",\"jenis\":\"4\",\"created_at\":\"2019-11-20 06:24:41\",\"updated_at\":\"2019-11-20 06:24:47\"},{\"id\":42,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":61,\"jawaban\":\"e\",\"jenis\":\"4\",\"created_at\":\"2019-11-20 06:24:41\",\"updated_at\":\"2019-11-20 06:24:48\"},{\"id\":43,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":62,\"jawaban\":\"e\",\"jenis\":\"4\",\"created_at\":\"2019-11-20 06:24:41\",\"updated_at\":\"2019-11-20 06:24:50\"},{\"id\":44,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":63,\"jawaban\":\"e\",\"jenis\":\"4\",\"created_at\":\"2019-11-20 06:24:41\",\"updated_at\":\"2019-11-20 06:24:51\"},{\"id\":45,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":64,\"jawaban\":\"e\",\"jenis\":\"4\",\"created_at\":\"2019-11-20 06:24:41\",\"updated_at\":\"2019-11-20 06:24:52\"},{\"id\":46,\"user_id\":8,\"userujian_id\":1,\"soaltestiq_id\":65,\"jawaban\":\"e\",\"jenis\":\"4\",\"created_at\":\"2019-11-20 06:24:41\",\"updated_at\":\"2019-11-20 06:24:53\"}]', '2019-11-16 23:53:59', '2019-11-19 23:25:54', '2019-11-19 18:00:00', '2019-11-20 18:00:00', '2019-11-20 13:24:42', '2019-11-20 13:22:27', '2019-11-20 13:24:41', '2019-11-20 13:24:23', '2019-11-20 13:26:42'),
(2, '8', 2, 0, NULL, NULL, '0', '0', NULL, '2019-11-16 23:53:59', '2019-11-18 11:00:00', '2019-11-19 18:00:00', '2019-11-20 18:00:00', NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indeks untuk tabel `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indeks untuk tabel `soal_testiqs`
--
ALTER TABLE `soal_testiqs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `soal_testiq_rumus`
--
ALTER TABLE `soal_testiq_rumus`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `soal_testiq_temps`
--
ALTER TABLE `soal_testiq_temps`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ujian_jeniss`
--
ALTER TABLE `ujian_jeniss`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indeks untuk tabel `user_ujians`
--
ALTER TABLE `user_ujians`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `soal_testiqs`
--
ALTER TABLE `soal_testiqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT untuk tabel `soal_testiq_rumus`
--
ALTER TABLE `soal_testiq_rumus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT untuk tabel `soal_testiq_temps`
--
ALTER TABLE `soal_testiq_temps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT untuk tabel `ujian_jeniss`
--
ALTER TABLE `ujian_jeniss`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `user_ujians`
--
ALTER TABLE `user_ujians`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
