import $axios from '../api.js'

const state = () => ({
    QuestionsOfPapiKostickTest: [], //UNTUK MENAMPUNG DATA SOAL
    temps: [], //UNTUK MENAMPUNG DATA
    page: 1, //PAGE AKTIF
    id: '' //NANTI AKAN DIGUNAKAN UNTUK EDIT DATA
})

const mutations = {
	//MEMASUKKAN DATA YANG DITERIMA KE DALAM STATE SOAL
    ASSIGN_DATA(state, payload) {
        state.QuestionsOfPapiKostickTest = payload
    },

    //MENGUBAH STATE PAGE
    SET_PAGE(state, payload) {
        state.page = payload
    },
    //MENGUBAH STATE ID
    SET_ID_UPDATE(state, payload) {
        state.id = payload
    }
}

const actions = {
	//FUNGSI INI AKAN MELAKUKAN REQUEST KE SERVER UNTUK MENGAMBILD ATA
    getQuestion({ commit, state }, payload) {
        let search = typeof payload != 'undefined' ? payload:''
        return new Promise((resolve, reject) => {
            //DENGAN MENGGUNAKAN AXIOS METHOD GET
            $axios.get(`/papi-kostick-test?page=${state.page}&q=${search}`)
            .then((response) => {
                //KEMUDIAN DI COMMIT UNTUK MELAKUKAN PERUBAHAN STATE
                commit('ASSIGN_DATA', response.data)
                resolve(response.data)
            })
        })
    },

    submitQuestion({ dispatch, commit }, payload) {
        return new Promise((resolve, reject) => {
            //MENGIRIMKAN PERMINTAAN KE SERVER DENGAN METHOD POST
            $axios.post(`/papi-kostick-test`, payload, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then((response) => {
                //KETIKA BERHASIL, MAKA DILAKUKAN REQUEST UNTUK MENGAMBIL DATA JOB SEEKER TERBARU
                dispatch('getQuestion').then(() => {
                    resolve(response.data)
                })
            })
            .catch((error) => {
                //JIKA GAGALNYA VALIDASI MAKA ERRONYA AKAN DI ASSIGN
                if (error.response.status == 422) {
                    commit('SET_ERRORS', error.response.data.errors, { root: true })
                }
            })
        })
    },

    editQuestion({ commit }, payload) {
        return new Promise((resolve, reject) => {
            $axios.get(`/papi-kostick-test/${payload}/edit`)
            .then((response) => {
                resolve(response.data)
            })
        })
    },

    updateQuestion({ state }, payload) {
    	return new Promise((resolve, reject) => {
    		this.axios.put(`/papi-kostick-test/${state.id}`, payload)
    		.then((response) => {
    			resolve(response.data)
    		})
    	})
    },

    removeQuestion({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            $axios.delete(`/papi-kostick-test/${payload}`)
            .then((response) => {
                dispatch('getQuestion').then(() => resolve(response.data))
            })
        })
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}