import $axios from '../api.js'

const state = () => ({
    soaltestiqs: [], //UNTUK MENAMPUNG DATA SOAL
    temps: [], //UNTUK MENAMPUNG DATA
    page: 1, //PAGE AKTIF
    id: '' //NANTI AKAN DIGUNAKAN UNTUK EDIT DATA
})

const mutations = {
	//MEMASUKKAN DATA YANG DITERIMA KE DALAM STATE SOAL
    ASSIGN_DATA(state, payload) {
        state.soaltestiqs = payload
    },

    //MENGUBAH STATE PAGE
    SET_PAGE(state, payload) {
        state.page = payload
    },
    //MENGUBAH STATE ID
    SET_ID_UPDATE(state, payload) {
        state.id = payload
    }
}

const actions = {
	//FUNGSI INI AKAN MELAKUKAN REQUEST KE SERVER UNTUK MENGAMBILD ATA
    getSoalTestIQ({ commit, state }, payload) {
        let search = typeof payload != 'undefined' ? payload:''
        return new Promise((resolve, reject) => {
            //DENGAN MENGGUNAKAN AXIOS METHOD GET
            $axios.get(`/soaltestiqs?page=${state.page}&q=${search}`)
            .then((response) => {
                //KEMUDIAN DI COMMIT UNTUK MELAKUKAN PERUBAHAN STATE
                commit('ASSIGN_DATA', response.data)
                resolve(response.data)
            })
        })
    },

    submitSoalTestIQ({ dispatch, commit }, payload) {
        return new Promise((resolve, reject) => {
            //MENGIRIMKAN PERMINTAAN KE SERVER DENGAN METHOD POST
            $axios.post(`/soaltestiqs`, payload, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then((response) => {
                //KETIKA BERHASIL, MAKA DILAKUKAN REQUEST UNTUK MENGAMBIL DATA JOB SEEKER TERBARU
                dispatch('getSoalTestIQ').then(() => {
                    resolve(response.data)
                })
            })
            .catch((error) => {
                //JIKA GAGALNYA VALIDASI MAKA ERRONYA AKAN DI ASSIGN
                if (error.response.status == 422) {
                    commit('SET_ERRORS', error.response.data.errors, { root: true })
                }
            })
        })
    },

    editSoalTestIQ({ commit }, payload) {
        return new Promise((resolve, reject) => {
            $axios.get(`/soaltestiqs/${payload}/edit`)
            .then((response) => {
                resolve(response.data)
            })
        })
    },

    updateSoalTestIQ({ state }, payload) {
    	return new Promise((resolve, reject) => {
    		$axios.post(`/soaltestiqs/${state.id}`, payload, {
    			headers: {
    				'Content-Type': 'multipart/form-data'
    			}
    		})
    		.then((response) => {
    			resolve(response.data)
    		})
    	})
    },

    removeSoalTestIQ({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            $axios.delete(`/soaltestiqs/${payload}`)
            .then((response) => {
                dispatch('getSoalTestIQ').then(() => resolve(response.data))
            })
        })
    },

    penilaianTestIQ({ commit }, payload){
        return new Promise((resolve, reject) => {
            $axios.get(`/soaltestiqs/penilaiantestiq/${payload}`)
            .then((response) => {
                resolve(response.data)
            })
        })
    },

    prosesTestIQ({ dispatch, commit }, payload){
        return new Promise((resolve, reject) => {
            $axios.get(`/soaltestiqs/prosestestiq/${payload}`)
            .then((response) => {
                resolve(response.data)
                //dispatch('penilaianTestIQ').then(() => resolve(response.data))
            })
        })
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}