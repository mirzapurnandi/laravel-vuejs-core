import $axios from '../api.js'

const state = () => ({
    jobseekers: [], //UNTUK MENAMPUNG DATA job seekers
    page: 1, //PAGE AKTIF
    id: '', //NANTI AKAN DIGUNAKAN UNTUK EDIT DATA
    testlists: [] //Untuk menampung Data Test Lists per masing masing Job Seekers
})

const mutations = {
    //MEMASUKKAN DATA YANG DITERIMA KE DALAM STATE jobseekers
    ASSIGN_DATA(state, payload) {
        state.jobseekers = payload
    },
    //MENGUBAH STATE PAGE
    SET_PAGE(state, payload) {
        state.page = payload
    },
    //MENGUBAH STATE ID
    SET_ID_UPDATE(state, payload) {
        state.id = payload
    },

    ASSIGN_DATA_TESTLIST(state, payload){
        state.testlists = payload
    },

    ASSIGN_DATA_SCORELIST_OF_TEST(state, payload){
        state.scorelists = payload
    }
}

const actions = {
    //FUNGSI INI AKAN MELAKUKAN REQUEST KE SERVER UNTUK MENGAMBILD ATA
    getJobSeekers({ commit, state }, payload) {
        let search = typeof payload != 'undefined' ? payload:''
        return new Promise((resolve, reject) => {
            //DENGAN MENGGUNAKAN AXIOS METHOD GET
            $axios.get(`/jobseekers?page=${state.page}&q=${search}`)
            .then((response) => {
                //KEMUDIAN DI COMMIT UNTUK MELAKUKAN PERUBAHA STATE
                commit('ASSIGN_DATA', response.data)
                resolve(response.data)
            })
        })
    },
    submitJobSeeker({ dispatch, commit }, payload) {
        return new Promise((resolve, reject) => {
            //MENGIRIMKAN PERMINTAAN KE SERVER DENGAN METHOD POST
            $axios.post(`/jobseekers`, payload, {
                //KARENA TERDAPAT FILE FOTO, MAKA HEADERNYA DITAMBAHKAN multipart/form-data
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then((response) => {
                //KETIKA BERHASIL, MAKA DILAKUKAN REQUEST UNTUK MENGAMBIL DATA JOB SEEKER TERBARU
                dispatch('getJobSeekers').then(() => {
                    resolve(response.data)
                })
            })
            .catch((error) => {
                //JIKA GAGALNYA VALIDASI MAKA ERRONYA AKAN DI ASSIGN
                if (error.response.status == 422) {
                    commit('SET_ERRORS', error.response.data.errors, { root: true })
                }
            })
        })
    },

    editJobSeeker({ commit }, payload) {
        return new Promise((resolve, reject) => {
            $axios.get(`/jobseekers/${payload}/edit`)
            .then((response) => {
                resolve(response.data)
            })
        })
    },

    updateJobSeeker({ state }, payload) {
        return new Promise((resolve, reject) => {
            this.axios.put(`/jobseekers/${state.id}`, payload)
            .then((response) => {
                resolve(response.data)
            })
        })
    },

    removeJobSeeker({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            $axios.delete(`/jobseekers/${payload}`)
            .then((response) => {
                dispatch('getJobSeekers').then(() => resolve(response.data))
            })
        })
    },

    viewTestList( { commit, state }, payload) {
        return new Promise((resolve, reject) => {
            $axios.get(`/jobseekers/test-list/${payload}`)
            .then((response) => {
                commit('ASSIGN_DATA_TESTLIST', response.data)
                resolve(response.data)
            })
        })
    },

    setTestLists({ dispatch, commit }, payload) {
        // show_btn_please_wait();
        return new Promise((resolve, reject) => {
            //MENGIRIMKAN PERMINTAAN KE SERVER DENGAN METHOD POST
            $axios.post(`/jobseekers/test-list/set`, payload)
            .then((response) => {
                commit('ASSIGN_DATA_TESTLIST', response.data)
                // console.log(response);
                // hide_btn_please_wait();
            })
            .catch((error) => {
                //JIKA GAGALNYA VALIDASI MAKA ERRONYA AKAN DI ASSIGN
                if (error.response.status == 422) {
                    // commit('SET_ERRORS', error.response.data.errors, { root: true })
                }
                // hide_btn_please_wait();
            })
        })
    },

    viewScoresOfTestLists({ commit, state }, payload){
        return new Promise((resolve, reject) => {
            $axios.get(`/jobseekers/test-list/view-scores/${payload}`)
            .then((response) => {
                console.log(response.data)
                commit('ASSIGN_DATA_SCORELIST_OF_TEST', response.data.data)
                resolve(response.data.data)
            })
            .catch((error) => {
                console.log(error.response)
            })
        })
    }
}

function show_btn_please_wait(){
    document.getElementById('btn-login-form-disabled').style.display = "block";
    document.getElementById('btn-login-form').style.display = "none";
}

function hide_btn_please_wait(){
    document.getElementById('btn-submit-form').style.display = "block";
    document.getElementById('btn-submit-form-disabled').style.display = "none";
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}