import $axios from '../api.js'

const state = () => ({

})

const mutations = {
    
}

const actions = {
    submit({ commit }, payload) {
        document.getElementById('btn-login-form-disabled').style.display = "block";
        document.getElementById('btn-login-form').style.display = "none";

        localStorage.setItem('token', null) //RESET LOCAL STORAGE MENJADI NULL
        commit('SET_TOKEN', null, { root: true }) //RESET STATE TOKEN MENJADI NULL
        //KARENA MUTATIONS SET_TOKEN BERADA PADA ROOT STORES, MAKA DITAMBAHKAN PARAMETER
        //{ root: true }
      
        //KITA MENGGUNAKAN PROMISE AGAR FUNGSI SELANJUTNYA BERJALAN KETIKA FUNGSI INI SELESAI
        return new Promise((resolve, reject) => {
            //MENGIRIM REQUEST KE SERVER DENGAN URI /login 
            //DAN PAYLOAD ADALAH DATA YANG DIKIRIMKAN DARI COMPONENT LOGIN.VUE
            $axios.post('/login', payload)
            .then((response) => {
                //KEMUDIAN JIKA RESPONNYA SUKSES
                if (response.data.status == 'success') {
                    //MAKA LOCAL STORAGE DAN STATE TOKEN AKAN DISET MENGGUNAKAN
                    //API DARI SERVER RESPONSE
                    localStorage.setItem('token', response.data.data)
                    localStorage.getItem('token');
                    commit('SET_TOKEN', response.data.data, { root: true })
                } else {
                    commit('SET_ERRORS', { invalid: 'Username/Password Salah' }, { root: true })

                    hide_btn_please_wait();
                }
                //JANGAN LUPA UNTUK MELAKUKAN RESOLVE AGAR DIANGGAP SELESAI
                resolve(response.data)
            })
            .catch((error) => {
                if (error.response.status == 422) {
                    commit('SET_ERRORS', { invalid: error.response.data.errors }, { root: true })
                }else if (error.response.status == 500) {
                    commit('SET_ERRORS', { invalid: "Gagal. Tidak dapat terhubung ke server" }, { root: true })
                }else{
                    commit('SET_ERRORS', { invalid: error.response.data.errors }, { root: true })
                }

                hide_btn_please_wait();
            })
        })
        
    }
}

function hide_btn_please_wait(){
    document.getElementById('btn-login-form').style.display = "block";
    document.getElementById('btn-login-form-disabled').style.display = "none";
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}