import $axios from '../api.js'

const state = () => ({
    ujians: [], //UNTUK MENAMPUNG DATA
    jawabans: [], //UNTUK MENAMPUNG JAWABAN
    page: 1, //PAGE AKTIF
    countdown: 1, //PAGE AKTIF
    id: '' //NANTI AKAN DIGUNAKAN UNTUK EDIT DATA
})

const mutations = {
    //MEMASUKKAN DATA YANG DITERIMA KE DALAM STATE jobseekers
    ASSIGN_DATA(state, payload) {
        state.ujians = payload
    },

    ASSIGN_JAWABAN(state, payload) {
        state.jawabans = payload
    },

    ASSIGN_NUMBER_OF_QUESTIONS(state, payload){
        state.number_of_questions = payload
    },

    ASSIGN_COUNTDOWN(state, payload) {
        state.countdown = payload
    },
    //MENGUBAH STATE PAGE
    SET_PAGE(state, payload) {
        state.page = payload
    },
    //MENGUBAH STATE ID
    SET_ID(state, payload) {
        state.id = payload
    }
}

const actions = {
    //FUNGSI INI AKAN MELAKUKAN REQUEST KE SERVER UNTUK MENGAMBILD ATA
    getHomes({ commit, state }, payload) {
        let search = typeof payload != 'undefined' ? payload:''
        return new Promise((resolve, reject) => {
            //DENGAN MENGGUNAKAN AXIOS METHOD GET
            $axios.get(`/homes?page=${state.page}&q=${search}`)
            .then((response) => {
                //KEMUDIAN DI COMMIT UNTUK MELAKUKAN PERUBAHA STATE
                commit('ASSIGN_DATA', response.data)
                resolve(response.data)
            })
        })
    },
    
    editHome({ commit }, payload) {
        return new Promise((resolve, reject) => {
            $axios.get(`/homes/${payload}/edit`)
            .then((response) => {
                resolve(response.data.data)
            })
        })
    },

    getUjian({ commit }, payload) {
        return new Promise((resolve, reject) => {
            $axios.get(`/homes/ujian/${payload}`)
            .then((response) => {
                commit('ASSIGN_DATA', response.data.data)
                commit('ASSIGN_COUNTDOWN', response.data.countdown)
                resolve(response.data)
            })
        })
    },

    getUjian2({ commit }, payload) {
        return new Promise((resolve, reject) => {
            $axios.post(`/homes/ujian2`, payload)
            .then((response) => {
                commit('ASSIGN_DATA', response.data.data)
                commit('ASSIGN_COUNTDOWN', response.data.countdown)
                resolve(response.data)
            })
        })
    },

    getJawaban({ commit }, payload) {
        return new Promise((resolve, reject) => {
            $axios.post(`/homes/jawaban`, payload)
            .then((response) => {
                resolve(response)
            })
            .catch((error) => {
                if (error.response.status == 422) {
                    commit('SET_ERRORS', error.response.data.errors, { root: true })
                }
            })
        })
    },

    handleEnd({commit}, payload){
        return new Promise((resolve, reject) => {
            $axios.get(`/homes/selesai/${payload}`)
            .then((response) => {
                resolve(response.data)
            })
        })
    },

    startTest({ commit }, payload) {
        return new Promise((resolve, reject) => {
            $axios.post(`/homes/start-test`, payload)
            .then((response) => {
                commit('ASSIGN_DATA', response.data.data)
                commit('ASSIGN_COUNTDOWN', response.data.countdown)
                commit('ASSIGN_NUMBER_OF_QUESTIONS', response.data.number_of_questions)
                resolve(response.data)
            })
        })
    },

    finishTest({ commit }, payload){
        return new Promise((resolve, reject) => {
            $axios.post(`/homes/finish-test`, payload)
            .then((response) => {
                resolve(response.data)
            })
            .catch((error) => {
                console.log(error.response)
            })
        })  
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}