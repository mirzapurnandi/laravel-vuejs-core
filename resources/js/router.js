//IMPORT SECTION
import Vue from 'vue'
import Router from 'vue-router'
import Home from './pages/home/Index.vue'
import Login from './pages/Login.vue'

import store from './store.js'

import Setting from './pages/setting/Index.vue'
import SetPermission from './pages/setting/roles/SetPermission.vue'

import IndexJobseeker from './pages/jobseekers/Index.vue'
import DataJobseekers from './pages/jobseekers/Jobseeker.vue'
import AddJobSeekers from './pages/jobseekers/Add.vue'
import EditJobSeekers from './pages/jobseekers/Edit.vue'
import NilaiJobSeekers from './pages/jobseekers/Nilai.vue'
import AssessmentJobSeekers from './pages/jobseekers/Assessment.vue'

import IndexSoal from './pages/soal/test-iq/Index.vue'
import SoalTestIQ from './pages/soal/test-iq/Data.vue'
import AddSoalTestIQ from './pages/soal/test-iq/Add.vue'
import EditSoalTestIQ from './pages/soal/test-iq/Edit.vue'

import IndexPapiKostickTest from './pages/soal/papi-kostick/Index.vue'
import QuestionOfPapiKostickTest from './pages/soal/papi-kostick/Data.vue'
import AddQuestionOfPapiKostickTest from './pages/soal/papi-kostick/Add.vue'
import EditQuestionOfPapiKostickTest from './pages/soal/papi-kostick/Edit.vue'

import IndexUjian from './pages/ujian/Index.vue'
import IndexNothing from './pages/ujian/Nothing.vue'
import UjianTestIQ from './pages/ujian/test-iq/Data.vue'
import UjianPapiKostickTest from './pages/ujian/papi-kostick-test/Data.vue'

Vue.use(Router)

//DEFINE ROUTE
const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            meta: { 
                requiresAuth: true, 
                title: 'Beranda'
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/setting',
            component: Setting,
            meta: { requiresAuth: true },
            children: [
                {
                    path: 'role-permission',
                    name: 'role.permissions',
                    component: SetPermission,
                    meta: { title: 'Set Permissions' }
                },
            ]
        },
        {
            path: '/jobseekers',
            component: IndexJobseeker,
            meta: { requiresAuth: true },
            children: [
                {
                    path: '',
                    name: 'jobseekers.data',
                    component: DataJobseekers,
                    meta: { title: 'Manage Job Seekers' }
                },
                {
                    path: 'add',
                    name: 'jobseekers.add',
                    component: AddJobSeekers,
                    meta: { title: 'Add New Job Seekers' }
                },
                {
                    path: 'nilai/:id',
                    name: 'jobseekers.nilai',
                    component: NilaiJobSeekers,
                    meta: { title: 'Penilaian Job Seekers' }
                },
                {
                    path: 'edit/:id',
                    name: 'jobseekers.edit',
                    component: EditJobSeekers,
                    meta: { title: 'Edit Data Job Seekers' }
                },
                {
                    path: 'assessment/:id',
                    name: 'jobseekers.seeAssessment',
                    component: AssessmentJobSeekers,
                    meta: { title: 'Penilaian Job Seekers' }
                }
            ]
        },
        {
            path: '/soaltestiqs',
            component: IndexSoal,
            meta: { requiresAuth: true },
            children: [
                {
                    path: '',
                    name: 'soaltestiqs.data',
                    component: SoalTestIQ,
                    meta: { title: 'Soal - Soal Test IQ'}    
                },
                {
                    path: 'add',
                    name: 'soaltestiqs.add',
                    component: AddSoalTestIQ,
                    meta: { title: 'Tambah Soal Test IQ'}    
                },
                {
                    path: 'edit/:id',
                    name: 'soaltestiqs.edit',
                    component: EditSoalTestIQ,
                    meta: { title: 'Edit Soal Test IQ'}    
                }
            ]
        },
        {
            path: '/papi-kostick-test',
            component: IndexPapiKostickTest,
            meta: { requiresAuth: true },
            children: [
                {
                    path: '',
                    name: 'QuestionsOfPapiKostickTest.data',
                    component: QuestionOfPapiKostickTest,
                    meta: { title: 'Soal - Soal Tes Papi Kostick'}    
                },
                {
                    path: 'add',
                    name: 'QuestionsOfPapiKostickTest.add',
                    component: AddQuestionOfPapiKostickTest,
                    meta: { title: 'Tambah Soal Tes Papi Kostick'}    
                },
                {
                    path: 'edit/:id',
                    name: 'QuestionsOfPapiKostickTest.edit',
                    component: EditQuestionOfPapiKostickTest,
                    meta: { title: 'Edit Soal Tes Papi Kostick'}    
                }
            ]
        },
        {
            path: '/ujians',
            component: IndexUjian,
            meta: { requiresAuth: true },
            children: [
                {
                    path: '',
                    name: 'nothing.data',
                    component: IndexNothing,
                    meta: { title: 'Kosong'}    
                },
                {
                    path: 'ujiantestiq/:id',
                    name: 'ujiantestiq',
                    component: UjianTestIQ,
                    meta: { title: 'Ujian Test IQ'}    
                },
                {
                    path: 'papi-kostick-test',
                    name: 'Ujian.PapiKostickTest',
                    component: UjianPapiKostickTest,
                    meta: { title: 'Ujian Tes Papi Kostick'}    
                }
            ]
        }
    ]
});

//Navigation Guards
router.beforeEach((to, from, next) => {
    store.commit('CLEAR_ERRORS')
    if (to.matched.some(record => record.meta.requiresAuth)) {
        let auth = store.getters.isAuth
        if (!auth) {
            next({ name: 'login' })
        } else {
            next()
        }
    } else {
        next()
    }
})

export default router