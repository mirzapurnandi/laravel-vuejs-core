<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SoalTestiqRumusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
        	[
        		'benar' => 0, 'nilai' => 38,
        	],
        	[
        		'benar' => 1, 'nilai' => 40,
        	],
        	[
        		'benar' => 2, 'nilai' => 43,
        	],
        	[
        		'benar' => 3, 'nilai' => 45,
        	],
        	[
        		'benar' => 4, 'nilai' => 47,
        	],
        	[
        		'benar' => 5, 'nilai' => 49,
        	],
        	[
        		'benar' => 6, 'nilai' => 52,
        	],
        	[
        		'benar' => 7, 'nilai' => 55,
        	],
        	[
        		'benar' => 8, 'nilai' => 57,
        	],
        	[
        		'benar' => 9, 'nilai' => 60,
        	],
        	[
        		'benar' => 10, 'nilai' => 63,
        	],
        	[
        		'benar' => 11, 'nilai' => 67,
        	],
        	[
        		'benar' => 12, 'nilai' => 70,
        	],
        	[
        		'benar' => 13, 'nilai' => 72,
        	],
        	[
        		'benar' => 14, 'nilai' => 75,
        	],
        	[
        		'benar' => 15, 'nilai' => 78,
        	],
        	[
        		'benar' => 16, 'nilai' => 81,
        	],
        	[
        		'benar' => 17, 'nilai' => 85,
        	],
        	[
        		'benar' => 18, 'nilai' => 88,
        	],
        	[
        		'benar' => 19, 'nilai' => 91,
        	],
        	[
        		'benar' => 20, 'nilai' => 94,
        	],
        	[
        		'benar' => 21, 'nilai' => 96,
        	],
        	[
        		'benar' => 22, 'nilai' => 100,
        	],
        	[
        		'benar' => 23, 'nilai' => 103,
        	],
        	[
        		'benar' => 24, 'nilai' => 106,
        	],
        	[
        		'benar' => 25, 'nilai' => 109,
        	],
        	[
        		'benar' => 26, 'nilai' => 113,
        	],
        	[
        		'benar' => 27, 'nilai' => 116,
        	],
        	[
        		'benar' => 28, 'nilai' => 119,
        	],
        	[
        		'benar' => 29, 'nilai' => 121,
        	],
        	[
        		'benar' => 30, 'nilai' => 124,
        	],
        	[
        		'benar' => 31, 'nilai' => 128,
        	],
        	[
        		'benar' => 32, 'nilai' => 131,
        	],
        	[
        		'benar' => 33, 'nilai' => 133,
        	],
        	[
        		'benar' => 34, 'nilai' => 137,
        	],
        	[
        		'benar' => 35, 'nilai' => 140,
        	],
        	[
        		'benar' => 36, 'nilai' => 142,
        	],
        	[
        		'benar' => 37, 'nilai' => 145,
        	],
        	[
        		'benar' => 38, 'nilai' => 149,
        	],
        	[
        		'benar' => 39, 'nilai' => 152,
        	],
        	[
        		'benar' => 40, 'nilai' => 155,
        	],
        	[
        		'benar' => 41, 'nilai' => 157,
        	],
        	[
        		'benar' => 42, 'nilai' => 161,
        	],
        	[
        		'benar' => 43, 'nilai' => 165,
        	],
        	[
        		'benar' => 44, 'nilai' => 167,
        	],
        	[
        		'benar' => 45, 'nilai' => 169,
        	],
        	[
        		'benar' => 46, 'nilai' => 173,
        	],
        	[
        		'benar' => 47, 'nilai' => 176,
        	],
        	[
        		'benar' => 48, 'nilai' => 179,
        	],
        	[
        		'benar' => 49, 'nilai' => 183,
        	],
        	[
        		'benar' => 50, 'nilai' => 183,
        	]
        );

        DB::table('soal_testiq_rumus')->insert($data);
    }
}
