<?php

use Illuminate\Database\Seeder;
use App\IqTestQuestionTypes;

class IqTestQuestionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = new IqTestQuestionTypes();
        $table->id = 1;
        $table->name = 'Series';
        $table->number_of_questions = 12;
        $table->time_limit = 180;
        $table->save();

        $table = new IqTestQuestionTypes();
        $table->id = 2;
        $table->name = 'Clasification';
        $table->number_of_questions = 14;
        $table->time_limit = 240;
        $table->save();

        $table = new IqTestQuestionTypes();
        $table->id = 3;
        $table->name = 'Matrices';
        $table->number_of_questions = 12;
        $table->time_limit = 180;
        $table->save();

        $table = new IqTestQuestionTypes();
        $table->id = 4;
        $table->name = 'Conditions / Topology';
        $table->number_of_questions = 8;
        $table->time_limit = 150;
    }
}
