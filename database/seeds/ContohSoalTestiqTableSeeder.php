<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ContohSoalTestiqTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        // Series
        for($i = 1; $i <= 15; $i++){
	        DB::table('soal_testiqs')->insert([
	        	'soal' 		=> $faker->text($maxNbChars = 200),
				'pilihan_a'	=> $faker->name,
				'pilihan_b'	=> $faker->name,
				'pilihan_c'	=> $faker->name,
				'pilihan_d'	=> $faker->name,
				'pilihan_e'	=> $faker->name,
				'pilihan_f'	=> $faker->name,
				'jawaban'	=> $faker->randomElement($array = array ('a','b','c','d','e','f'),
				'jenis'		=> 1
			]);
	    }

        // Clasification
        for($j = 1; $j <= 18; $j++){
	        DB::table('soal_testiqs')->insert([
				'pilihan_a'	=> $faker->address,
				'pilihan_b'	=> $faker->address,
				'pilihan_c'	=> $faker->address,
				'pilihan_d'	=> $faker->address,
				'pilihan_e'	=> $faker->address,
				'jawaban'	=> $faker->randomElement($array = array ('a','b','c','d','e')),
				'jenis'		=> 2
			]);
	    }

	    // Matrices
        for($k = 1; $k <= 15; $k++){
	        DB::table('soal_testiqs')->insert([
	        	'soal' 		=> $faker->text($maxNbChars = 180),
				'pilihan_a'	=> $faker->name,
				'pilihan_b'	=> $faker->name,
				'pilihan_c'	=> $faker->name,
				'pilihan_d'	=> $faker->name,
				'pilihan_e'	=> $faker->name,
				'pilihan_f'	=> $faker->name,
				'jawaban'	=> $faker->randomElement($array = array ('a','b','c','d','e','f'),
				'jenis'		=> 3
			]);
	    }

	    // Conditions / Topology
        for($l = 1; $l <= 18; $l++){
	        DB::table('soal_testiqs')->insert([
	        	'soal' 		=> $faker->text($maxNbChars = 220),
				'pilihan_a'	=> $faker->address,
				'pilihan_b'	=> $faker->address,
				'pilihan_c'	=> $faker->address,
				'pilihan_d'	=> $faker->address,
				'pilihan_e'	=> $faker->address,
				'jawaban'	=> $faker->randomElement($array = array ('a','b','c','d','e')),
				'jenis'		=> 4
			]);
	    }
    }
}
