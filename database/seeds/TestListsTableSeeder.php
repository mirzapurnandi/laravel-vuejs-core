<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Flynsarmy\CsvSeeder\CsvSeeder;

class TestListsTableSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'test_lists';
        $this->csv_delimiter = ';';
        $this->filename = base_path().'/resources/csv/TestLists.csv';
        $this->offset_rows = 1;
        $this->mapping = [
            0 => 'id',
            1 => 'name',
            2 => 'number_of_questions',
            3 => 'time_limit',
            4 => 'type',
            5 => 'status',
            6 => 'parent_id',
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        parent::run();
    }
}
