<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UjianJenissTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
        	[
        		'name' => 'Test Ketik',
        		'status' => '1',
        	],
        	[
        		'name' => 'Test Administrasi (Excel)',
        		'status' => '1',
        	],
        	[
        		'name' => 'Test IQ',
        		'status' => '1',
        	],
        	[
        		'name' => 'Test Papi Kostick',
        		'status' => '1',
        	],
        	[
        		'name' => 'Test DISC',
        		'status' => '1',
        	],
        	[
        		'name' => 'Test Deret Angka (Kraepelin)',
        		'status' => '1',
        	]
        	
        );

        DB::table('ujian_jeniss')->insert($data);
    }
}
