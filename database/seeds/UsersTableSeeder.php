<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Administrator RPO',
            'username' => 'admin',
            'email' => 'admin@rpoindonesia.com',
            'email_verified_at' => now(),
            'password' => bcrypt('123admin'),
            'role' => 0
        ]);
    }
}
