<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        // $this->call(ContohSoalTestiqTableSeeder::class);
        $this->call(SoalTestiqJenisTableSeeder::class);
        $this->call(SoalTestiqRumusTableSeeder::class);
        $this->call(UjianJenissTableSeeder::class);

        $this->call(TestListsTableSeeder::class);

        $this->call(IqTestQuestionTypesTableSeeder::class);

        $this->call(PapiKostickTestQuestionsTableSeeder::class);
    }
}
