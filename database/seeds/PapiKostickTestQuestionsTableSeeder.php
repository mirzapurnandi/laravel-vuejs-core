<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Flynsarmy\CsvSeeder\CsvSeeder;

class PapiKostickTestQuestionsTableSeeder extends CsvSeeder
{
	public function __construct()
    {
        $this->table = 'papi_kostick_test_questions';
        $this->csv_delimiter = ';';
        $this->filename = base_path().'/resources/csv/PapiKostick.csv';
        $this->offset_rows = 1;
        $this->mapping = [
            0 => 'first_statement',
            1 => 'first_aspect',
            2 => 'second_statement',
            3 => 'second_aspect',
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        parent::run();
    }
}
