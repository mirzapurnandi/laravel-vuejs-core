<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\SoalTestiqJenis;

class SoalTestiqJenisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $table = new SoalTestiqJenis();
        $table->id = 1;
        $table->name = 'Series';
        $table->jumlah_soal = "12";
        $table->waktu = "180";
        $table->save();

        $table = new SoalTestiqJenis();
        $table->id = 2;
        $table->name = 'Clasification';
        $table->jumlah_soal = "14";
        $table->waktu = "240";
        $table->save();

        $table = new SoalTestiqJenis();
        $table->id = 3;
        $table->name = 'Matrices';
        $table->jumlah_soal = "12";
        $table->waktu = "180";
        $table->save();

        $table = new SoalTestiqJenis();
        $table->id = 4;
        $table->name = 'Conditions / Topology';
        $table->jumlah_soal = "8";
        $table->waktu = "150";
        $table->save();

    }
}
