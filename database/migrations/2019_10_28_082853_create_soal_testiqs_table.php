<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoalTestiqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soal_testiqs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('soal')->nullable();
            $table->string('soal_gambar')->nullable();
            $table->text('pilihan_a')->nullable();
            $table->string('pilihan_a_gambar')->nullable();
            $table->text('pilihan_b')->nullable();
            $table->string('pilihan_b_gambar')->nullable();
            $table->text('pilihan_c')->nullable();
            $table->string('pilihan_c_gambar')->nullable();
            $table->text('pilihan_d')->nullable();
            $table->string('pilihan_d_gambar')->nullable();
            $table->text('pilihan_e')->nullable();
            $table->string('pilihan_e_gambar')->nullable();
            $table->text('pilihan_f')->nullable();
            $table->string('pilihan_f_gambar')->nullable();
            $table->enum('jawaban', ['a', 'b', 'c', 'd', 'e', 'f'])->default('a');
            $table->enum('jenis', ['1', '2', '3', '4'])->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soal_testiqs');
    }
}
