<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoalTestiqJenisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soal_testiq_jenis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('jumlah_soal');
            $table->integer('waktu')->comment('waktu pengerjaan, mis. 3 menit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soal_testiq_jenis');
    }
}
