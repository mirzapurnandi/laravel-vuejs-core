<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePapiKostickTestQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('papi_kostick_test_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->longText('first_statement');
            $table->string('first_aspect');
            $table->longText('second_statement');
            $table->string('second_aspect');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('papi_kostick_test_questions');
    }
}
