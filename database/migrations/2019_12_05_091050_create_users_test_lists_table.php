<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTestListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_test_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('test_list_id');
            $table->integer('number_of_questions')->nullable();
            $table->longText('answers')->nullable();
            $table->integer('right_answers')->nullable();
            $table->integer('wrong_answers')->nullable();
            $table->enum('status', ['0', '1', '2'])->comment('0: Active, 1: Live, 2: Done')->default('0');
            $table->string('score')->default(0)->nullable();
            $table->text('test_list')->nullable();
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->dateTime('test_start')->nullable();
            $table->dateTime('first_test_end')->nullable();
            $table->dateTime('second_test_end')->nullable();
            $table->dateTime('third_test_end')->nullable();
            $table->dateTime('fourth_test_end')->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('test_list_id')->references('id')->on('test_lists')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_test_lists');
    }
}
