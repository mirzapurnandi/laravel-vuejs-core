<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserUjiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_ujians', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id');
            $table->integer('ujianjenis_id');
            $table->integer('jumlah');
            $table->integer('benar')->nullable();
            $table->integer('salah')->nullable();
            $table->enum('status', ['0', '1', '2'])->comment('0: aktif, 1: berlangsung, 2: selesai')->default('0');
            $table->string('score')->default(0);
            $table->text('list_soal');
            $table->dateTime('waktu_mulai');
            $table->dateTime('waktu_akhir');
            $table->dateTime('ujian_mulai');
            $table->dateTime('ujian_akhir');
            $table->dateTime('ujian_akhir2');
            $table->dateTime('ujian_akhir3');
            $table->dateTime('ujian_akhir4');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_ujians');
    }
}
