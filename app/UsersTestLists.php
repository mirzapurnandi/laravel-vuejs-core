<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class UsersTestLists extends Model
{
    protected $guarded = [];

    protected $dates = [
        'start_time',
        'end_time',
        'test_start',
        'first_test_start',
    ];

    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function testlist(){
        return $this->belongsTo(TestList::class);
    }

    public function scopeSelectTestLists(Builder $builder)
    {
    	return $builder->select('id', 'user_id', 'test_list_id', 'answers', 'right_answers', 'wrong_answers', 'status', 'score')
            ->addSelect([
                'test_list_name' => TestList::select('name')
                    ->whereColumn('test_lists.id', 'users_test_lists.test_list_id')
                    ->limit(1),
                'test_list_number_of_questions' => TestList::select('number_of_questions')
                    ->whereColumn('test_lists.id', 'users_test_lists.test_list_id')
                    ->limit(1),
                'test_list_time_limit' => TestList::select('time_limit')
                    ->whereColumn('test_lists.id', 'users_test_lists.test_list_id')
                    ->limit(1),
                'test_list_type' => TestList::select('type')
                    ->whereColumn('test_lists.id', 'users_test_lists.test_list_id')
                    ->limit(1),
                'test_list_parent_id' => TestList::select('parent_id')
                    ->whereColumn('test_lists.id', 'users_test_lists.test_list_id')
                    ->limit(1)
            ]);
    }
}