<?php

namespace App\Http\Middleware;

use Closure;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //get token via header
        $token = $request->header('Authorization');
        if(empty($token)){
            return response()->json([
                'error' => 'Authorization Header is empty'
            ]);
        }

        //format bearer token : 
        //Bearer[spasi]randomhashtoken 
        $pecah_token = explode(" ", $token);
        if(count($pecah_token) <> 2){
            return response()->json([
                'error' => 'Invalid Authorization format'
            ]);
        }

        if(trim($pecah_token[0]) <> 'Bearer'){
            return response()->json([
                'error' => 'Authorization header must be a Bearer'
            ]);
        }

        $access_token = trim($pecah_token[1]);

        //cek apakah access_token ini ada di database atau tidak
        //$cek = TokenManagement::where('access_token', $access_token)->first();
        if($access_token != "==ApiRPOIndonesia-mirza=="){
            return response()->json([
                'error' => 'Forbidden : Invalid access token'
            ]);
        }

        return $next($request);
    }
}
