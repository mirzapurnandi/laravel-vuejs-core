<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UsersTestLists;
use App\SoalTestiq;
use App\SoalTestiqTemp;
use App\Http\Resources\HomeCollection;
use Carbon\Carbon;
use App\TestList;
use App\PapiKostickTestQuestion;

class HomeController extends Controller
{
    public function index()
    {
    	$user  = request()->user()->id;
    	$ujian = UsersTestLists::selectujianjenis()->orderBy('created_at', 'DESC')->where('user_id', $user);
        if (request()->q != '') {
            $ujian = $ujian->where('number_of_questions', 'LIKE', '%' . request()->q . '%');
        }

        $ujian = $ujian->paginate(10);

        $ujian = $ujian->map(function($item){
            $start_time = Carbon::now('+07:00');
            $end_time = new Carbon($item['end_time'], '+07:00');
            if(strtotime($end_time) > strtotime($start_time)){
                if($item->status == 0){
                    $countdown = $start_time->diffInSeconds($end_time) * 1000;
                } else {
                    $countdown = 0;
                }
            } else {
                $countdown = 0;
            }

            $item->countdown = $countdown;
            return $item;
        });

        return new HomeCollection($ujian);
    }

    public function edit($id)
    {
        $UsersTestLists = UsersTestLists::findOrFail($id);

        /*if($UsersTestLists->test_list_id == 3)
        {
            $soal = SoalTestiq::take($UsersTestLists->number_of_questions)->get();
            foreach ($soal as $key => $val) {             
                SoalTestiqTemp::create([
                    'user_id'       => $UsersTestLists->user_id,
                    'userujian_id'  => $UsersTestLists->id,
                    'soaltestiq_id'  => $val->id
                ]);
            }
        }*/

        $tanggal_now = Carbon::now('+07:00');

        //Update Status menjadi 1 agar tampilan berubah
        $UsersTestLists->status = '1';
        $UsersTestLists->test_start = $tanggal_now;
        //$UsersTestLists->ujian_akhir = $tanggal_now->addMinutes(50);
        $UsersTestLists->save();
        //Untuk menampilkan soal ujian
        //$UsersTestLists = SoalTestiqTemp::where('user_id', 8)->Where('userujian_id', 1)->get();

        return response()->json(['status' => 'success', 'data' => $UsersTestLists]);
    }

    public function ujian2(Request $request)
    {
        $user = request()->user()->id;

        //insert soal 
        $UsersTestLists = UsersTestLists::findOrFail($request->id);

        if($UsersTestLists->test_list_id == 1)
        {
            $soal = SoalTestiq::where('jenis', $request->instruksi)->take($request->number_of_questions)->get();
            foreach ($soal as $key => $val) {             
                SoalTestiqTemp::create([
                    'user_id'       => $UsersTestLists->user_id,
                    'userujian_id'  => $UsersTestLists->id,
                    'soaltestiq_id' => $val->id,
                    'jenis'         => $request->instruksi
                ]);
            }
        }

        $tanggal_now = Carbon::now('+07:00');
        $UsersTestLists->ujian_mulai = $tanggal_now;
        if($request->instruksi == 1){
            $UsersTestLists->ujian_akhir = $tanggal_now->addMinutes($request->menit);
        } elseif($request->instruksi == 2){
            $UsersTestLists->ujian_akhir2 = $tanggal_now->addMinutes($request->menit);
        } elseif($request->instruksi == 3){
            $UsersTestLists->ujian_akhir3 = $tanggal_now->addMinutes($request->menit);
        } elseif($request->instruksi == 4){
            $UsersTestLists->ujian_akhir4 = $tanggal_now->addMinutes($request->menit);
        }
        $UsersTestLists->save();


        //$temp = [];
        if($request->instruksi == 1) {
            $ujian_akhir    = new Carbon($UsersTestLists->ujian_akhir, '+07:00');
            $countdown      = 180000; //$tanggal_now->diffInSeconds($ujian_akhir) * 1000;

            /*if(strtotime($ujian_akhir) > strtotime($tanggal_now)){
                if($UsersTestLists->status == 1){
                    $countdown = $tanggal_now->diffInSeconds($ujian_akhir) * 1000;
                } else {
                    $countdown = 600000;
                }
            } else {
                $countdown = 1200000;
            }*/

            //$temp = SoalTestiqTemp::with('soalTestiq')->where('user_id', $user)->where('userujian_id', $request->id)->where('jenis', $request->instruksi)->get();
        } elseif($request->instruksi == 2){
            $ujian_akhir    = new Carbon($UsersTestLists->ujian_akhir2, '+07:00');
            $countdown      = 240000;
        } elseif($request->instruksi == 3){
            $ujian_akhir    = new Carbon($UsersTestLists->ujian_akhir3, '+07:00');
            $countdown      = 180000;
        } elseif($request->instruksi == 4){
            $ujian_akhir    = new Carbon($UsersTestLists->ujian_akhir4, '+07:00');
            $countdown      = 150000;
        }

        $temp = SoalTestiqTemp::with('soalTestiq')->where('user_id', $user)->where('userujian_id', $request->id)->where('jenis', $request->instruksi)->get();

        return response()->json(['status' => 'success', 'data' => $temp, 'instruksi' => $request->instruksi, 'countdown' => $countdown]);






        /*$user = request()->user()->id;
        $temp = SoalTestiqTemp::with('soalTestiq')->where('user_id', $user)->where('userujian_id', $request->id)->get();

        $UsersTestLists = UsersTestLists::findOrFail($id);

        $ujian_mulai = Carbon::now('+07:00');
        $ujian_akhir = new Carbon($UsersTestLists->ujian_akhir, '+07:00');

        if(strtotime($ujian_akhir) > strtotime($ujian_mulai)){
            if($UsersTestLists->status == 1){
                $countdown = $ujian_mulai->diffInSeconds($ujian_akhir) * 1000;
            } else {
                $countdown = 0;
            }
        } else {
            $countdown = 0;
        }
        

        return response()->json(['status' => 'success', 'data' => $temp, 'countdown' => $countdown]);*/
    }

    /*public function ujian($id)
    {
        $user = request()->user()->id;
        $temp = SoalTestiqTemp::with('soalTestiq')->where('user_id', $user)->Where('userujian_id', $id)->get();

        $UsersTestLists = UsersTestLists::findOrFail($id);

        $ujian_mulai = Carbon::now('+07:00');
        $ujian_akhir = new Carbon($UsersTestLists->ujian_akhir, '+07:00');

        if(strtotime($ujian_akhir) > strtotime($ujian_mulai)){
            if($UsersTestLists->status == 1){
                $countdown = $ujian_mulai->diffInSeconds($ujian_akhir) * 1000;
            } else {
                $countdown = 0;
            }
        } else {
            $countdown = 0;
        }
        

        return response()->json(['status' => 'success', 'data' => $temp, 'countdown' => $countdown]);
    }*/

    public function jawaban(Request $request){
        $user = request()->user()->id;

        $soal = SoalTestiqTemp::where('id', $request->id)
            ->where('user_id', $user)
            ->update(['jawaban' => $request->jawaban]);

        return response()->json(['status' => 'success', 'data' => $soal]);
    }

    public function selesai($id){
        $user = request()->user()->id;
        $UsersTestLists = UsersTestLists::findOrFail($id);

        $UsersTestLists->status = '2';
        $UsersTestLists->save();

        return response()->json(['status' => 'success']);
    }

    /*public function testing()
    {
        $ujian_mulai = Carbon::now('+07:00');
        $ujian_akhir = new Carbon('2019-11-12 12:00:00', '+07:00');

        if(strtotime($ujian_akhir) > strtotime($ujian_mulai)){
            $countdown = $ujian_mulai->diffInSeconds($ujian_akhir) * 1000;
        } else {
            $countdown = 0;
        }


        $user  = 8;
        $ujian = UsersTestLists::selectujianjenis()->orderBy('created_at', 'DESC')->where('user_id', $user)
            ->addSelect('
                IF(ujian_mulai is null, TIMEDIFF(end_time, NOW()), TIMEDIFF(ujian_akhir, NOW())) as countdown
            ');
        if (request()->q != '') {
            $ujian = $ujian->where('number_of_questions', 'LIKE', '%' . request()->q . '%');
        }
        $ujian = $ujian->paginate(10);

        return [
            ['ujian' => $ujian],
            ['countdown' => $countdown]
        ];
        //return new HomeCollection($ujian);
    }*/

    public function startTest(Request $request)
    {
        $user_id = request()->user()->id;

        $test_list_id = $request->test_list_id;

        $UsersTestLists = UsersTestLists::where([
            'user_id' => $user_id,
            'test_list_id' => $test_list_id,
        ])->first();

        if($UsersTestLists == ""){
            return response()->json(['status' => 'error', 'message' => 'Data tidak ditemukan']);
        }else{
            $TestList = TestList::select('id', 'number_of_questions', 'time_limit')
                ->where('id', $test_list_id)->first();

            $date_now = Carbon::now('+07:00');

            $number_of_questions = $TestList->number_of_questions;

            if($test_list_id == 1){
                $soal = SoalTestiq::where('jenis', $request->instruksi)->take($request->number_of_questions)->get();
                foreach ($soal as $key => $val) {             
                    SoalTestiqTemp::create([
                        'user_id'       => $user_id,
                        'userujian_id'  => $UsersTestLists->id,
                        'soaltestiq_id' => $val->id,
                        'jenis'         => $request->instruksi
                    ]);
                }

                $instruction = $request->instruksi;

                if($instruction == 1){
                    $first_test_end = $date_now->addMinutes($request->menit);
                } elseif($instruction == 2){
                    $second_test_end = $date_now->addMinutes($request->menit);
                } elseif($instruction == 3){
                    $third_test_end = $date_now->addMinutes($request->menit);
                } elseif($instruction == 4){
                    $fourth_test_end = $date_now->addMinutes($request->menit);
                }

                if($instruction == 1) {
                    $test_end    = new Carbon($UsersTestLists->first_test_end, '+07:00');
                    $countdown      = 180000; //$date_now->diffInSeconds($test_end) * 1000;

                } elseif($instruction == 2){
                    $test_end    = new Carbon($UsersTestLists->second_test_end, '+07:00');
                    $countdown      = 240000;
                } elseif($instruction == 3){
                    $test_end    = new Carbon($UsersTestLists->third_test_end, '+07:00');
                    $countdown      = 180000;
                } elseif($instruction == 4){
                    $test_end    = new Carbon($UsersTestLists->fourth_test_end, '+07:00');
                    $countdown      = 150000;
                }

                $temp = SoalTestiqTemp::with('soalTestiq')->where('user_id', $user_id)->where('userujian_id', $request->id)->where('jenis', $instruction)->get();

            }else{
                $time_limit = $TestList->time_limit; //In Second
                $minutes = $time_limit/60;

                $first_test_end = $date_now->addMinutes($minutes);

                $instruction = 1;
                $countdown = $time_limit*1000;

                $temp = [];
                if($test_list_id == 2){
                    $temp = PapiKostickTestQuestion::get();
                }

                $data_update = [
                    'first_test_end' => $first_test_end,
                ];

                try{
                    $UsersTestLists::where('id', $UsersTestLists->id)
                        ->update($data_update);
                } catch (\Exception $e) {
                    return response()->json(['status' => 'error', 'data' => $e->getMessage()], 200);
                }
            }

            return response()->json(['status' => 'success', 
                'data' => $temp, 
                'instruction' => $instruction, 
                'countdown' => $countdown,
                'number_of_questions' => $number_of_questions
            ]);
        }
    }

    public function finishTest(Request $request){
        $user_id = request()->user()->id;

        $test_list_id = $request->test_list_id;
        $answer = $request->answer;

        $UsersTestLists = UsersTestLists::where([
            'user_id' => $user_id,
            'test_list_id' => $test_list_id,
        ])->first();

        if($UsersTestLists == ""){
            return response()->json(['status' => 'error', 'message' => 'Data tidak ditemukan']);
        }else{
            $data_update = [
                'status' => '2',
                'answers' => $answer,
            ];

            try{
                $UsersTestLists::where('id', $UsersTestLists->id)
                    ->update($data_update);
            } catch (\Exception $e) {
                return response()->json(['status' => 'error', 'data' => $e->getMessage()], 200);
            }
        }

        return response()->json(['status' => 'success']);
    }
}
