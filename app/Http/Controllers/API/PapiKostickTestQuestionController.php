<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PapiKostickTestQuestion;
use App\Http\Resources\SoalCollection;

class PapiKostickTestQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $question = PapiKostickTestQuestion::orderBy('created_at', 'ASC');
        if (request()->q != '') {
            $question = $question->where('first_statement', 'LIKE', '%' . request()->q . '%')
                ->orWhere('second_statement', 'LIKE', '%' . request()->q . '%');
        }
        $question = $question->paginate(10);
        return new SoalCollection($question);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = PapiKostickTestQuestion::findOrFail($id);
        return response()->json(['status' => 'success', 'data' => $question], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_statement' => 'required',
            'first_aspect' => 'required',
            'second_statement' => 'required',
            'second_aspect' => 'required'
        ]);

        try {
            $question = PapiKostickTestQuestion::findOrFail($id);

            $question->first_statement = $request->first_statement;
            $question->first_aspect = $request->first_aspect;
            $question->second_statement = $request->second_statement;
            $question->second_aspect = $request->second_aspect;

            $question->save();

            return response()->json(['status' => 'success'], 200);
            
        } catch (Exception $e) {
            return response()->json(['status' => 'error', 'data' => $e->getMessage()], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
