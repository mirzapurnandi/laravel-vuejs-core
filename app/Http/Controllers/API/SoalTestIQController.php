<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\SoalCollection;
use App\SoalTestiq;
use App\SoalTestiqTemp;
use App\SoalTestiqRumus;
use App\UserUjian;
use Carbon\Carbon;
use DB;
use File;

class SoalTestIQController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $soal = SoalTestiq::orderBy('created_at', 'DESC');
        if (request()->q != '') {
            $soal = $soal->where('soal', 'LIKE', '%' . request()->q . '%');
        }
        $soal = $soal->paginate(10);
        return new SoalCollection($soal);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$this->validate($request, [
            'soal'              => 'required|min:6',
            'soal_gambar'       => 'image',
            'pilihan_a_gambar'  => 'image',
            'pilihan_b_gambar'  => 'image',
            'pilihan_c_gambar'  => 'image',
            'pilihan_d_gambar'  => 'image',
            'pilihan_e_gambar'  => 'image',
            'jawaban'           => 'required'
        ]);*/

        DB::beginTransaction();
        try {
            $soal_gambar = NULL;
            if($request->hasFile('soal_gambar')){
                $filesoal = $request->file('soal_gambar');
                $soal_gambar = time() .'_soal.'. $filesoal->getClientOriginalExtension();
                $filesoal->storeAs('public/soal', $soal_gambar);
            }

            $pilihan_a_gambar = NULL;
            if($request->hasFile('pilihan_a_gambar')){
                $filea = $request->file('pilihan_a_gambar');
                $pilihan_a_gambar = time() .'_a.'. $filea->getClientOriginalExtension();
                $filea->storeAs('public/soal', $pilihan_a_gambar);
            }

            $pilihan_b_gambar = NULL;
            if($request->hasFile('pilihan_b_gambar')){
                $fileb = $request->file('pilihan_b_gambar');
                $pilihan_b_gambar = time() .'_b.'. $fileb->getClientOriginalExtension();
                $fileb->storeAs('public/soal', $pilihan_b_gambar);
            }

            $pilihan_c_gambar = NULL;
            if($request->hasFile('pilihan_c_gambar')){
                $filec = $request->file('pilihan_c_gambar');
                $pilihan_c_gambar = time() .'_c.'. $filec->getClientOriginalExtension();
                $filec->storeAs('public/soal', $pilihan_c_gambar);
            }

            $pilihan_d_gambar = NULL;
            if($request->hasFile('pilihan_d_gambar')){
                $filed = $request->file('pilihan_d_gambar');
                $pilihan_d_gambar = time() .'_d.'. $filed->getClientOriginalExtension();
                $filed->storeAs('public/soal', $pilihan_d_gambar);
            }

            $pilihan_e_gambar = NULL;
            if($request->hasFile('pilihan_e_gambar')){
                $filee = $request->file('pilihan_e_gambar');
                $pilihan_e_gambar = time() .'_e.'. $filee->getClientOriginalExtension();
                $filee->storeAs('public/soal', $pilihan_e_gambar);
            }

            $pilihan_f_gambar = NULL;
            if($request->hasFile('pilihan_f_gambar')){
                $filef = $request->file('pilihan_f_gambar');
                $pilihan_f_gambar = time() .'_f.'. $filef->getClientOriginalExtension();
                $filee->storeAs('public/soal', $pilihan_f_gambar);
            }

            $soal = New SoalTestiq;
            $soal->soal             = $request->soal;
            $soal->soal_gambar      = $soal_gambar;
            $soal->pilihan_a        = $request->pilihan_a;
            $soal->pilihan_a_gambar = $pilihan_a_gambar;
            $soal->pilihan_b        = $request->pilihan_b;
            $soal->pilihan_b_gambar = $pilihan_b_gambar;
            $soal->pilihan_c        = $request->pilihan_c;
            $soal->pilihan_c_gambar = $pilihan_c_gambar;
            $soal->pilihan_d        = $request->pilihan_d;
            $soal->pilihan_d_gambar = $pilihan_d_gambar;
            $soal->pilihan_e        = $request->pilihan_e;
            $soal->pilihan_e_gambar = $pilihan_e_gambar;
            $soal->pilihan_f        = $request->pilihan_f;
            $soal->pilihan_f_gambar = $pilihan_f_gambar;
            $soal->jawaban          = $request->jawaban;
            $soal->jenis            = $request->jenis;
            $soal->save();

            DB::commit();
            return response()->json(['status' => 'success'], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'error', 'data' => $e->getMessage()], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $soal = SoalTestiq::findOrFail($id);
        return response()->json(['status' => 'success', 'data' => $soal], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'soal'              => 'required|min:6',
            'jawaban'           => 'required'
        ]);

        try {
            $soal = SoalTestiq::findOrFail($id);
            
            $soal_gambar = $soal->soal_gambar;
            if($request->hasFile('soal_gambar')){
                $filesoal = $request->file('soal_gambar');
                File::delete(storage_path('app/public/soal/' . $soal_gambar));
                $soal_gambar = time() .'_soal.'. $filesoal->getClientOriginalExtension();
                $filesoal->storeAs('public/soal', $soal_gambar);
            }

            $pilihan_a_gambar = $soal->pilihan_a_gambar;
            if($request->hasFile('pilihan_a_gambar')){
                $filea = $request->file('pilihan_a_gambar');
                File::delete(storage_path('app/public/soal/' . $pilihan_a_gambar));
                $pilihan_a_gambar = time() .'_a.'. $filea->getClientOriginalExtension();
                $filea->storeAs('public/soal', $pilihan_a_gambar);
            }

            $pilihan_b_gambar = $soal->pilihan_b_gambar;
            if($request->hasFile('pilihan_b_gambar')){
                $fileb = $request->file('pilihan_b_gambar');
                File::delete(storage_path('app/public/soal/' . $pilihan_b_gambar));
                $pilihan_b_gambar = time() .'_b.'. $fileb->getClientOriginalExtension();
                $fileb->storeAs('public/soal', $pilihan_b_gambar);
            }

            $pilihan_c_gambar = $soal->pilihan_c_gambar;
            if($request->hasFile('pilihan_c_gambar')){
                $filec = $request->file('pilihan_c_gambar');
                File::delete(storage_path('app/public/soal/' . $pilihan_c_gambar));
                $pilihan_c_gambar = time() .'_c.'. $filec->getClientOriginalExtension();
                $filec->storeAs('public/soal', $pilihan_c_gambar);
            }

            $pilihan_d_gambar = $soal->pilihan_d_gambar;
            if($request->hasFile('pilihan_d_gambar')){
                $filed = $request->file('pilihan_d_gambar');
                File::delete(storage_path('app/public/soal/' . $pilihan_d_gambar));
                $pilihan_d_gambar = time() .'_d.'. $filed->getClientOriginalExtension();
                $filed->storeAs('public/soal', $pilihan_d_gambar);
            }

            $pilihan_e_gambar = $soal->pilihan_e_gambar;
            if($request->hasFile('pilihan_e_gambar')){
                $filee = $request->file('pilihan_e_gambar');
                File::delete(storage_path('app/public/soal/' . $pilihan_e_gambar));
                $pilihan_e_gambar = time() .'_e.'. $filee->getClientOriginalExtension();
                $filee->storeAs('public/soal', $pilihan_e_gambar);
            }

            $soal->soal             = $request->soal;
            $soal->soal_gambar      = $soal_gambar;
            $soal->pilihan_a        = $request->pilihan_a == 'null' ? null : $request->pilihan_a;
            $soal->pilihan_a_gambar = $pilihan_a_gambar;
            $soal->pilihan_b        = $request->pilihan_b == 'null' ? null : $request->pilihan_b;
            $soal->pilihan_b_gambar = $pilihan_b_gambar;
            $soal->pilihan_c        = $request->pilihan_c == 'null' ? null : $request->pilihan_c;
            $soal->pilihan_c_gambar = $pilihan_c_gambar;
            $soal->pilihan_d        = $request->pilihan_d == 'null' ? null : $request->pilihan_d;
            $soal->pilihan_d_gambar = $pilihan_d_gambar;
            $soal->pilihan_e        = $request->pilihan_e == 'null' ? null : $request->pilihan_e;
            $soal->pilihan_e_gambar = $pilihan_e_gambar;
            $soal->jawaban          = $request->jawaban;
            $soal->save();

            return response()->json(['status' => 'success'], 200);
        } catch (Exception $e) {
            return response()->json(['status' => 'error', 'data' => $e->getMessage()], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $soal = SoalTestiq::findOrFail($id);

        File::delete(storage_path('app/public/soal/' . $soal->soal_gambar));
        File::delete(storage_path('app/public/soal/' . $soal->pilihan_a_gambar));
        File::delete(storage_path('app/public/soal/' . $soal->pilihan_b_gambar));
        File::delete(storage_path('app/public/soal/' . $soal->pilihan_c_gambar));
        File::delete(storage_path('app/public/soal/' . $soal->pilihan_d_gambar));
        File::delete(storage_path('app/public/soal/' . $soal->pilihan_e_gambar));

        $soal->delete();
        return response()->json(['status' => 'success'], 200);
    }

    public function penilaiantestiq($id){
        $userujian = UserUjian::where('user_id', $id)->where('ujianjenis_id', 3)->first();

        if($userujian->list_soal == null){
            $temp  = SoalTestiqTemp::where('user_id', $id)->where('userujian_id', $userujian->id)->get();
        } else {
            $temp = json_decode($userujian->list_soal);
        }
        

        $arr_data = [];
        foreach ($temp as $key => $val) {
            $soal = SoalTestiq::where('id', $val->soaltestiq_id)->first();

            $arr_data[$key]['soaltestiq_id'] = $val->soaltestiq_id;
            $arr_data[$key]['jawaban'] = $val->jawaban;
            $arr_data[$key]['jawaban_benar'] = $soal->jawaban;
            $arr_data[$key]['pernyataan'] = $val->jawaban == $soal->jawaban ? 'Benar' : 'Salah';
        }

        return response()->json(['status' => 'success', 'data' => $userujian, 'temp' => $arr_data], 200);
    }

    public function prosestestiq($id){
        $userujian = UserUjian::where('user_id', $id)->where('ujianjenis_id', 3)->first();

        $temp  = SoalTestiqTemp::where('user_id', $id)->where('userujian_id', $userujian->id);
        $benar = 0;
        $salah = 0;
        $temp2 = $temp->get();
        foreach ($temp2 as $key => $val) {
            $soal = SoalTestiq::where('id', $val->soaltestiq_id)->first();
            $val->jawaban == $soal->jawaban ? $benar++ : $salah++;
        }

        $rumus = SoalTestiqRumus::all();
        $score = 0;
        foreach ($rumus as $keys => $vals) {
            if($benar == $vals->benar){
                $score = $vals->nilai;
                break;
            }
        }

        $userujian->update([
            'benar' => $benar,
            'salah' => $salah, 
            'score' => $score,
            'list_soal' => $temp2
        ]);

        $temp->delete();

        //## TRUNCATE ##//
        $trunc = SoalTestiqTemp::all();
        if($trunc->count() == 0){
            $trunc->truncate();
        }

        return response()->json(['status' => 'success', 'data' => $userujian], 200);
    }
}
