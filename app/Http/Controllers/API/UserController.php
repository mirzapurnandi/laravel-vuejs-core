<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserCollection;
use Spatie\Permission\Models\Permission;
use App\User;
use App\UserUjian;
use DB;
use Carbon\Carbon;

use App\UsersTestLists;
use App\TestList;

class UserController extends Controller
{
	public function index()
	{
		//$users = User::with(['outlet'])->orderBy('created_at', 'DESC')->jobseeker();
		$users = User::orderBy('created_at', 'DESC')->jobseeker();
		if (request()->q != '') {
			$users = $users->where('name', 'LIKE', '%' . request()->q . '%');
		}
		$users = $users->paginate(10);
		return new UserCollection($users);
	}

	public function userLists()
	{
		$user = User::where('role', '!=', 3)->get();
		return new UserCollection($user);
	}

	public function store(Request $request)
	{
		//VALIDASI
		$this->validate($request, [
			'name' 		=> 'required|string|max:150',
			'username' 	=> 'required|unique:users,username',
			'email' 	=> 'required|email|unique:users,email',
			'password' 	=> 'required|min:6|string'
		]);

		DB::beginTransaction();
		try {
			$name = NULL;
			//APABILA ADA FILE YANG DIKIRIMKAN

			if ($request->hasFile('photo')) {
				//MAKA FILE TERSEBUT AKAN DISIMPAN KE STORAGE/APP/PUBLIC/JOBSEEKERS
				$file = $request->file('photo');
				$name = $request->email . '-' . time() . '.' . $file->getClientOriginalExtension();
				$file->storeAs('public/jobseekers', $name);
			}

			//BUAT DATA BARUNYA KE DATABASE
			User::create([
				'name' 		=> $request->name,
				'username' 	=> $request->username,
				'email' 	=> $request->email,
				'password' 	=> bcrypt($request->password),
				'role' 		=> $request->role,
				'photo' 	=> $name,
				'role' 		=> 3 //Hanya untuk Job Seeker
			]);
			DB::commit();
			return response()->json(['status' => 'success'], 200);
		} catch (\Exception $e) {
			DB::rollback();
			return response()->json(['status' => 'error', 'data' => $e->getMessage()], 200);
		}
	}

	public function userregister(Request $request)
	{
		$user = new User();
		$user->name 	= $request->name;
		$user->username = $request->username;
		$user->email 	= $request->email;
		$user->password = bcrypt($request->password);
		$user->token 	= $request->token;
		$user->role 	= '3'; //Sebagai Job-Seeker
		$user->status 	= '0';
		$user->save();

		$user->assignRole('jobseeker');

		$arr_jenisujianid = json_decode($request->jenisujianid, true);
		foreach($arr_jenisujianid as $key => $val) {
			$user_ujian = new UserUjian();
			$user_ujian->userid 		= $user->id;
			$user_ujian->ujianjenisid 	= $key;
			$user_ujian->jumlah	 		= $val;
			$user_ujian->save();
		}

		return $user;
	}

	public function getUserLogin()
	{
		$user = request()->user();
		$permissions = [];
		foreach (Permission::all() as $permission) {
			if (request()->user()->can($permission->name)) {
				$permissions[] = $permission->name;
			}
		}
		$user['permission'] = $permissions;
		return response()->json(['status' => 'success', 'data' => $user]);
	}

	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return response()->json(['status' => 'success', 'data' => $user], 200);
    }

	public function destroy($id)
    {
    	$user = User::findOrFail($id);
    	$user->delete();

    	return response()->json(['status' => 'success'], 200);
    }

    /*public function penilaian($id){
    	$user = User::findOrFail($id);

    	return new UserCollection($user);
    }*/

    public function test_lists($id){
    	$test_lists = TestList::select('id', 'name', 'number_of_questions', 'time_limit', 'parent_id',
    			DB::raw('(SELECT users_test_lists.status FROM users_test_lists 
    				WHERE users_test_lists.test_list_id = test_lists.id 
    					AND users_test_lists.user_id = '.$id.') AS test_status'
    			)
    		)
    		->where('parent_id', NULL)->get();

    	return $test_lists;
    }

    public function get_test_list($id){
    	$test_lists = $this->test_lists($id);

    	return response()->json(['status' => 'success', 'id' => $id, 'data' => $test_lists], 200);
    }

    public function set_test_list(Request $request)
	{
		//VALIDASI
		$this->validate($request, [
			'user_id' 		=> 'required',
			'test_lists' 		=> 'required',
		]);

		$user_id = $request->user_id;
		$test_lists = $request->test_lists;

		try{
			$get_checked_test_lists = explode(',', $test_lists);
			$count_checked_test_lists = count($get_checked_test_lists);

			foreach($get_checked_test_lists as $n => $number){
				$test_list_id = $get_checked_test_lists[$n];

				$check_test_list = UsersTestLists::where([
					'user_id' => $user_id,
					'test_list_id' => $test_list_id
				])->first();

				if($check_test_list == null || empty($check_test_list) || $check_test_list == ""){

					$right_now = Carbon::now('+07:00');
					$in_24_hours_ahead = $right_now->addHours(24);

					$data = [
						'user_id' => $user_id,
						'test_list_id' => $test_list_id,
						'status' => '0',
						'start_time' => $right_now,
						'end_time' => $in_24_hours_ahead
					];

					$UsersTestLists = UsersTestLists::insert($data);
				}
			}

			$test_lists = $this->test_lists($user_id);

			return response()->json(['status' => 'success', 'data' => $test_lists], 200);

		} catch (\Exception $e) {
			DB::rollback();
			return response()->json(['status' => 'error', 'data' => $e->getMessage()], 200);
		}
	}

	public function get_score_list_of_test($id){
    	$TestLists = UsersTestLists::selectTestLists()
    		->where('user_id', $id)
    		->get();

    	return response()->json(['status' => 'success', 'id' => $id, 'data' => $TestLists], 200);
    }
}
