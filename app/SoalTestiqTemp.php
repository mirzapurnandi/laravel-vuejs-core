<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoalTestiqTemp extends Model
{
	protected $table = 'soal_testiq_temps';

	protected $guarded = [];

	public function soalTestiq()
    {
    	return $this->belongsTo(SoalTestiq::class, 'soaltestiq_id');
    }
}
