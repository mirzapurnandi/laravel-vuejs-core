<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PapiKostickTestQuestion extends Model
{
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
