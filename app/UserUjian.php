<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserUjian extends Model
{
    protected $table = 'user_ujians';

    protected $guarded = [];

    protected $dates = [
        'waktu_mulai',
        'waktu_akhir',
        'ujian_mulai',
        'ujian_akhir',
    ];

    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function scopeSelectUjianJenis($query)
    {
    	return $query->addSelect(['ujianjenis_name' => UjianJenis::select('name')
            ->whereColumn('ujian_jeniss.id', 'user_ujians.ujianjenis_id')
            ->limit(1)]);
    }
}
