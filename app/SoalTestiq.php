<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoalTestiq extends Model
{
	protected $table = 'soal_testiqs';
	
    protected $guarded = [];

    public function soalTestiqTemp()
    {
    	return $this->hasOne(SoalTestiqTemp::class);
    }
}
